#ifndef PRESENT_FACTORY_H
#define PRESENT_FACTORY_H

namespace game
{
	class Present;
	class PresentManager;

	class PresentFactory
	{
	public:
		static Present* CreatePresent(PresentManager* presentManager, int width, int height);
	};
}

#endif