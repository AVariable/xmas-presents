#ifndef BUTTON_H
#define BUTTON_H

#include <SDL.h>

namespace af
{
	class Texture;
	class Text;
	class Rectangle;
	class Renderer;
	class Sound;
}

namespace game
{
	class ButtonBehaviour;

	class Button
	{
	public:
		bool enabled;

	private:
		float x;
		float y;
		float xText;
		float yText;
		af::Texture* normalTexture;
		af::Texture* pressedTexture;
		af::Texture* currentTexture;
		af::Text* text;
		af::Rectangle* rectangle;
		bool pressed;
		ButtonBehaviour* buttonBehaviour;
		af::Sound* soundClick;

	public:
		Button(af::Texture* normalTexture);
		~Button();

		const float GetX();
		void SetX(float x);

		const float GetY();
		void SetY(float y);

		const float GetXText();
		void SetXText(float xText);

		const float GetYText();
		void SetYText(float yText);

		af::Texture* GetNormalTexture();
		void SetNormalTexture(af::Texture* normalTexture);

		af::Texture* GetPressedTexture();
		void SetPressedTexture(af::Texture* pressedTexture);

		af::Text* GetText();
		void SetText(af::Text* text);

		af::Rectangle* GetRectangle();
		void SetRectangle(af::Rectangle* rectangle);

		void SetButtonBehaviour(ButtonBehaviour* buttonBehaviour);

		af::Sound* GetSoundClick();
		void SetSoundClick(af::Sound* soundClick);

		void HandleInput(SDL_Event& e);
		void Render(af::Renderer* renderer);
	};
}

#endif