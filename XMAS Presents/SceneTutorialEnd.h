#ifndef SCENE_TUTORIAL_END_H
#define SCENE_TUTORIAL_END_H

#include <SDL.h>

#include "Scene.h"

namespace af
{
	class Texture;
	class Text;
	class Timer;
	class Renderer;
}

namespace game
{
	class SceneTutorialEnd : public Scene
	{
	private:
		static const int MAX_TIME;

		af::Texture* textureBalloon;
		af::Text* textBalloon1;
		af::Text* textBalloon2;
		af::Timer* timer;

	public:
		SceneTutorialEnd();
		~SceneTutorialEnd();

	protected:
		void Initialize();
		void HandleInput(SDL_Event& e);
		void Update();
		void Render(af::Renderer* renderer);
	};
}

#endif