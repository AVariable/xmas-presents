#ifndef ENGINE_H
#define ENGINE_H

namespace af
{
	class Application;
	class Window;
	class Renderer;
	class Content;

	class Engine
	{
		friend class Application;

	private:
		static Application* application;
		static Window* window;
		static Renderer* renderer;
		static Content* content;

	public:
		static Application* GetApplication();
		static Window* GetWindow();
		static Content* GetContent();
	};
}

#endif