#include "Scene.h"

#include "Renderer.h"
#include "SceneManager.h"

game::Scene::Scene()
{
	state = State::ON;
}

game::Scene::~Scene()
{
}

game::Scene::State game::Scene::GetState()
{
	return state;
}

void game::Scene::SetState(game::Scene::State state)
{
	this->state = state;
}