#ifndef SOUND_H
#define SOUND_H

#include <SDL_mixer.h>

namespace af
{
	class Sound
	{
	private:
		Mix_Chunk* sound;
		int volume;

	public:
		Sound(Mix_Chunk* sound);
		~Sound();

		int GetVolume();
		void SetVolume(int volume);

		// Plays the sound
		void Play();
	};
}

#endif