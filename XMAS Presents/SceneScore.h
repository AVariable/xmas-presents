#ifndef SCENE_SCORE_H
#define SCENE_SCORE_H

#include <SDL.h>

#include "Scene.h"

namespace af
{
	class Renderer;
	class Texture;
	class Text;
}

namespace game
{
	class Button;

	class SceneScore : public Scene
	{
	public:
		int score;
		int level;

	private:
		static const int N_PRESENTS = 15;
		static const int X_START_PRESENTS = 200;
		static const int Y_START_PRESENTS = 325;

		af::Texture* textureBG;
		Button* buttonContinue;
		Button* buttonQuit;
		int xPresents[N_PRESENTS];
		int yPresents[N_PRESENTS];
		af::Texture* texturePresents[N_PRESENTS];
		af::Texture* textureElf;
		af::Texture* textureBalloon;
		af::Text* textBalloon;
		af::Text* textLevel;
		af::Text* textScore;
		af::Text* textLevelNumber;
		af::Text* textScoreNumber;

	public:
		SceneScore();
		~SceneScore();

	protected:
		void Initialize();
		void HandleInput(SDL_Event& e);
		void Update();
		void Render(af::Renderer* renderer);
	};
}

#endif