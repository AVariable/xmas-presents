#include "PresentBag.h"

#include "PresentManager.h"
#include "Content.h"
#include "Engine.h"
#include "Texture.h"

game::PresentBag::PresentBag(PresentManager* presentManager, int width, int height)
	:Present(presentManager, width, height)
{
	af::Content* content = af::Engine::GetContent();

	type = rand() % 5 + 1;
	texture = new af::Texture(content->GetTexture("spriteSheet"), width, height, 226 - (type - 1) / 3 * 29, 95 + (type - 1) % 3 * 30, 29, 30);
	texture->SetColor(255, 255, 255, 255);
	texture->SetBlendMode(SDL_BLENDMODE_BLEND);
	score = 500;

	content = nullptr;
}

game::PresentBag::~PresentBag()
{
}

void game::PresentBag::GetPresentsOfType(int type, std::vector<Present*>& presents)
{
	Present** allPresents = presentManager->GetPresents();

	presents.push_back(this);

	// Loop through all the presents
	for (unsigned int i = 0; i < PresentManager::SIZE_PRESENTS; i++)
	{
		// If the present is not a null pointer
		if (allPresents[i] != nullptr)
		{
			bool exists = false;

			// Check if the present already exists in the presents list
			for (unsigned int j = 0; j < presents.size(); j++)
			{
				if (presents[j] == allPresents[i])
				{
					exists = true;
					break;
				}
			}

			// If the present doesn't exist in the list and is of the same type, save it in the list and get it's adjacents
			if (!exists && allPresents[i]->type == type)
			{
				presents.push_back(allPresents[i]);
			}
		}
	}
}