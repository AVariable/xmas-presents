#ifndef BUTTON_BEHAVIOUR_SCORE_QUIT_H
#define BUTTON_BEHAVIOUR_SCORE_QUIT_H

#include "ButtonBehaviour.h"

namespace game
{
	class ButtonBScoreQuit : virtual public ButtonBehaviour
	{
	public:
		ButtonBScoreQuit();
		~ButtonBScoreQuit();

		void OnClick();
	};
}

#endif