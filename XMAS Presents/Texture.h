#ifndef TEXTURE_H
#define TEXTURE_H

#include <SDL.h>

namespace af
{
	class Renderer;
	
	class Texture
	{
		friend class Renderer;
		
	private:
		SDL_Texture* texture;
		int width;
		int height;
		int srcX;
		int srcY;
		int srcWidth;
		int srcHeight;
		Uint8 red;
		Uint8 green;
		Uint8 blue;
		Uint8 alpha;
		SDL_BlendMode blendMode;

	public:
		Texture(SDL_Texture* texture, int width, int height, int srcX, int srcY, int srcWidth, int srcHeight);
		Texture(SDL_Texture* texture, int width, int height);
		Texture(SDL_Texture* texture);
		~Texture();

		const int GetWidth();
		const int GetHeight();
		const int GetSrcX();
		const int GetSrcY();
		const int GetSrcWidth();
		const int GetSrcHeight();
		void SetColor(Uint8 red, Uint8 green, Uint8 blue, Uint8 alpha);
		void SetBlendMode(SDL_BlendMode blendMode);

	private:
		void Initialize(SDL_Texture* texture, int width, int height, int srcX, int srcY, int srcWidth, int srcHeight);
	};
}

#endif