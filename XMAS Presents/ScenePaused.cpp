#include "ScenePaused.h"

#include "Texture.h"
#include "Renderer.h"
#include "Content.h"
#include "Engine.h"
#include "SceneManager.h"
#include "SceneGame.h"

game::ScenePaused::ScenePaused(SceneGame* sceneGame)
{
	this->sceneGame = sceneGame;
	sceneGame->PauseTimer();
}

game::ScenePaused::~ScenePaused()
{
	sceneGame = nullptr;
	delete textureBG;
	textureBG = nullptr;
}

void game::ScenePaused::Initialize()
{
	af::Content* content = af::Engine::GetContent();

	textureBG = new af::Texture(content->GetTexture("pausedBG"));
}

void game::ScenePaused::HandleInput(SDL_Event& e)
{
	if (e.type == SDL_MOUSEBUTTONDOWN)
	{
		if (e.button.button == SDL_BUTTON_LEFT)
		{
			SceneManager::Delete(this);
			sceneGame->SetState(State::ON);
			sceneGame->UnpauseTimer();
		}
	}
}

void game::ScenePaused::Update()
{

}

void game::ScenePaused::Render(af::Renderer* renderer)
{
	renderer->Draw(textureBG, 400, 300);
}