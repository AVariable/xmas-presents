#ifndef RECTANGLE_H
#define RECTANGLE_H

namespace af
{
	class Rectangle
	{
	public:
		int x;
		int y;
		int width;
		int height;

		Rectangle(int x, int y, int width, int height);
		~Rectangle();

		bool Intersects(const Rectangle* rectangle);
	};
}

#endif