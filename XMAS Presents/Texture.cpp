#include "Texture.h"

#include "Renderer.h"

af::Texture::Texture(SDL_Texture* texture, int width, int height, int srcX, int srcY, int srcWidth, int srcHeight)
{
	Initialize(texture, width, height, srcX, srcY, srcWidth, srcHeight);
}

af::Texture::Texture(SDL_Texture* texture, int width, int height)
{
	int srcWidth, srcHeight;

	SDL_QueryTexture(texture, nullptr, nullptr, &srcWidth, &srcHeight);
	Initialize(texture, width, height, 0, 0, srcWidth, srcHeight);
}

af::Texture::Texture(SDL_Texture* texture)
{
	int width, height;

	SDL_QueryTexture(texture, nullptr, nullptr, &width, &height);
	Initialize(texture, width, height, 0, 0, width, height);
}

af::Texture::~Texture()
{
	texture = nullptr;
}

const int af::Texture::GetWidth()
{
	return width;
}

const int af::Texture::GetHeight()
{
	return height;
}

const int af::Texture::GetSrcX()
{
	return srcX;
}

const int af::Texture::GetSrcY()
{
	return srcY;
}

const int af::Texture::GetSrcWidth()
{
	return srcWidth;
}

const int af::Texture::GetSrcHeight()
{
	return srcHeight;
}

void af::Texture::SetColor(Uint8 red, Uint8 green, Uint8 blue, Uint8 alpha)
{
	this->red = red;
	this->green = green;
	this->blue = blue;
	this->alpha = alpha;
}

void af::Texture::SetBlendMode(SDL_BlendMode blendMode)
{
	this->blendMode = blendMode;
}

void af::Texture::Initialize(SDL_Texture* texture, int width, int height, int srcX, int srcY, int srcWidth, int srcHeight)
{
	this->texture = texture;
	this->width = width;
	this->height = height;
	this->srcX = srcX;
	this->srcY = srcY;
	this->srcWidth = srcWidth;
	this->srcHeight = srcHeight;
	red = green = blue = 255;
	alpha = 0;
	blendMode = SDL_BLENDMODE_NONE;
}