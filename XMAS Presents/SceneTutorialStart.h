#ifndef SCENE_TUTORIAL_START_H
#define SCENE_TUTORIAL_START_H

#include <SDL.h>

#include "Scene.h"

namespace af
{
	class Text;
	class Renderer;
}

namespace game
{
	class SceneGame;

	class SceneTutorialStart : public Scene
	{
	private:
		SceneGame* sceneGame;
		af::Text* textTitle;
		af::Text* textClick;

	public:
		SceneTutorialStart(SceneGame* sceneGame);
		~SceneTutorialStart();

	protected:
		void Initialize();
		void HandleInput(SDL_Event& e);
		void Update();
		void Render(af::Renderer* renderer);
	};
}

#endif