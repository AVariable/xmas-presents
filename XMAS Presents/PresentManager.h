#ifndef PRESENT_MANAGER_H
#define PRESENT_MANAGER_H

#include <SDL.h>

namespace af
{
	class Renderer;
	class Sound;
}

namespace game
{
	class SceneGame;
	class Present;

	class PresentManager
	{
	public:
		static const unsigned int SIZE_PRESENTS = 60;

	private:
		static const unsigned int X_PRESENTS;
		static const unsigned int PRESENT_WIDTH;
		static const unsigned int PRESENT_HEIGHT;
		const float xStart;
		const float yStart;
		SceneGame* sceneGame;
		Present* presents[SIZE_PRESENTS];
		af::Sound* clickPresentSound;

	public:
		PresentManager(SceneGame* sceneGame, float xStart, float yStart);
		~PresentManager();

		SceneGame* GetSceneGame();

		Present** GetPresents();

		void ClearAndRespawn();

		void HandleInput(SDL_Event& e);
		void Update();
		void Render(af::Renderer* renderer);

		// Deletes all marked for deletion presents
		void DeleteAllMarked();

		// Repositions the presents in the array on the y axis
		void RepositionYAxis();

		// Repositions the presents in the array on the x axis
		void RepositionXAxis();

		// Clears all adjacencies of all the presents
		void ClearAllAdjacencies();

		// Recalculates adjacencies for all the presents
		void RecalculateAdjacencies();

		// Checks if the last column is occupied with presents
		bool IsLastColumnOccupied();

		// Spawns new presents in the first column
		void SpawnNewPresents();

		// Moves the presents to their respective positions on the grid
		void RepositionPresents();

		// Checks if an adjacency of the same type exists
		bool ExistsAdjacenciesOfSameType();

		// Checks if any present is transitioning
		bool CheckIfTransitioning();

	private:
		// Pushes all the columns forward one position
		void PushColumnsForward();
	};
}

#endif