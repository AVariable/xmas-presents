#include "SceneScore.h"

#include "Renderer.h"
#include "Texture.h"
#include "Engine.h"
#include "Content.h"
#include "Button.h"
#include "Text.h"
#include "Rectangle.h"
#include "ButtonBScoreQuit.h"
#include "ButtonBScoreContinue.h"
#include "Sound.h"

#include <iostream>

game::SceneScore::SceneScore()
{
}

game::SceneScore::~SceneScore()
{
	delete textureBG;
	textureBG = nullptr;
	delete buttonContinue;
	buttonContinue = nullptr;
	delete buttonQuit;
	buttonQuit = nullptr;
	delete textureElf;
	textureElf = nullptr;
}

void game::SceneScore::Initialize()
{
	af::Content* content = af::Engine::GetContent();
	af::Texture* btnNormalTexture = new af::Texture(content->GetTexture("spriteSheet"), 150, 41, 153, 27, 100, 27);
	af::Texture* btnPressedTexture = new af::Texture(content->GetTexture("spriteSheet"), 150, 41, 153, 0, 100, 27);

	textureBG = new af::Texture(content->GetTexture("background"));
	textureBG->SetBlendMode(SDL_BLENDMODE_BLEND);
	textureBG->SetColor(255, 255, 255, 255);

	af::Text* textQuit = new af::Text(content->GetFont("8bit16"));
	textQuit->SetColor(206, 186, 21, 0);
	textQuit->SetText("Quit");

	buttonQuit = new Button(btnNormalTexture);
	buttonQuit->SetX(670);
	buttonQuit->SetY(500);
	buttonQuit->SetRectangle(new af::Rectangle(595, 479, 150, 41));
	buttonQuit->SetPressedTexture(btnPressedTexture);
	buttonQuit->SetXText(670);
	buttonQuit->SetYText(500);
	buttonQuit->SetText(textQuit);
	buttonQuit->SetButtonBehaviour(new ButtonBScoreQuit());
	buttonQuit->SetSoundClick(new af::Sound(content->GetSound("clickButton")));

	af::Text* textContinue = new af::Text(content->GetFont("8bit16"));
	textContinue->SetColor(206, 186, 21, 0);
	textContinue->SetText("Continue");

	buttonContinue = new Button(btnNormalTexture);
	buttonContinue->SetX(670);
	buttonContinue->SetY(400);
	buttonContinue->SetRectangle(new af::Rectangle(595, 379, 150, 41));
	buttonContinue->SetPressedTexture(btnPressedTexture);
	buttonContinue->SetXText(670);
	buttonContinue->SetYText(400);
	buttonContinue->SetText(textContinue);
	buttonContinue->SetButtonBehaviour(new ButtonBScoreContinue());
	buttonContinue->SetSoundClick(new af::Sound(content->GetSound("clickButton")));

	int x = X_START_PRESENTS;
	int y = Y_START_PRESENTS;
	int maxPresents = 1;
	int currentPresents = 1;
	int xCurrentStart = X_START_PRESENTS;

	for (unsigned int i = 0; i < N_PRESENTS; i++)
	{
		int type = rand() % 5 + 1;

		af::Texture* texture = new af::Texture(content->GetTexture("spriteSheet"), 51, 53, (type - 1) % 3 * 51, (type - 1) / 3 * 53, 51, 53);
		texture->SetColor(255, 255, 255, 255);
		texture->SetBlendMode(SDL_BLENDMODE_BLEND);

		xPresents[i] = x;
		yPresents[i] = y;
		texturePresents[i] = texture;
		currentPresents--;

		if (currentPresents == 0)
		{
			maxPresents += 1;
			xCurrentStart -= 26;
			x = xCurrentStart;
			y += 40;
			currentPresents = maxPresents;
		}
		else
		{
			x += 51;
		}

		texture = nullptr;
	}

	textureElf = new af::Texture(content->GetTexture("spriteSheet"), 50, 85, 213, 54, 23, 39);
	textureElf->SetBlendMode(SDL_BLENDMODE_BLEND);
	textureElf->SetColor(255, 255, 255, 255);

	textureBalloon = new af::Texture(content->GetTexture("spriteSheet"), 250, 100, 138, 105, 50, 50);
	textureBalloon->SetBlendMode(SDL_BLENDMODE_BLEND);
	textureBalloon->SetColor(255, 255, 255, 255);

	textBalloon = new af::Text(content->GetFont("8bit16"));
	textBalloon->SetColor(0, 0, 0, 0);
	textBalloon->SetText("Game Over");

	textLevel = new af::Text(content->GetFont("8bit16"));
	textLevel->SetColor(255, 255, 255, 0);
	textLevel->SetText("Level");

	textScore = new af::Text(content->GetFont("8bit16"));
	textScore->SetColor(255, 255, 255, 255);
	textScore->SetText("Score");

	textLevelNumber = new af::Text(content->GetFont("8bit16"));
	textLevelNumber->SetColor(255, 255, 255, 0);
	textLevelNumber->SetText(std::to_string(level));

	textScoreNumber = new af::Text(content->GetFont("8bit16"));
	textScoreNumber->SetColor(255, 255, 255, 0);
	textScoreNumber->SetText(std::to_string(score));

	textContinue = nullptr;
	textQuit = nullptr;
	btnPressedTexture = nullptr;
	btnNormalTexture = nullptr;
	content = nullptr;
}

void game::SceneScore::HandleInput(SDL_Event& e)
{
	buttonQuit->HandleInput(e);
	buttonContinue->HandleInput(e);
}

void game::SceneScore::Update()
{
}

void game::SceneScore::Render(af::Renderer* renderer)
{
	renderer->Draw(textureBG, 400, 300);
	buttonQuit->Render(renderer);
	buttonContinue->Render(renderer);

	for (unsigned int i = 0; i < N_PRESENTS; i++)
	{
		renderer->Draw(texturePresents[i], xPresents[i], yPresents[i]);
	}
	renderer->Draw(textureElf, 200, 275);
	renderer->Draw(textureBalloon, 325, 210);
	renderer->DrawText(textBalloon, 340, 200);

	renderer->DrawText(textLevel, 100, 50);
	renderer->DrawText(textLevelNumber, 100, 75);

	renderer->DrawText(textScore, 400, 50);
	renderer->DrawText(textScoreNumber, 400, 75);
}