#ifndef SCENE_TUTORIAL_ELF_H
#define SCENE_TUTORIAL_ELF_H

#include <SDL.h>

#include "Scene.h"

namespace af
{
	class Renderer;
	class Texture;
	class Text;
	class Timer;
}

namespace game
{
	class SceneTutorialElf : public Scene
	{
	private:
		static const int MAX_TIME;

		af::Texture* textureBalloon;
		af::Text* textBalloon;
		af::Timer* timer;

	public:
		SceneTutorialElf();
		~SceneTutorialElf();

	protected:
		void Initialize();
		void HandleInput(SDL_Event& e);
		void Update();
		void Render(af::Renderer* renderer);
	};
}

#endif