#ifndef TEXT_H
#define TEXT_H

#include <SDL_ttf.h>
#include <string>

namespace af
{
	class Renderer;

	class Text
	{
		friend class Renderer;

	private:
		TTF_Font* font;
		std::string text;
		Uint8 red;
		Uint8 green;
		Uint8 blue;
		Uint8 alpha;

	public:
		Text(TTF_Font* font);
		~Text();

		std::string GetText();
		void SetText(std::string text);
		void SetColor(Uint8 red, Uint8 green, Uint8 blue, Uint8 alpha);
	};
}

#endif