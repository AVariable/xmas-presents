#include "SceneGame.h"

#include <string>

#include "Timer.h"
#include "Renderer.h"
#include "PresentManager.h"
#include "Button.h"
#include "Engine.h"
#include "Content.h"
#include "Application.h"
#include "Text.h"
#include "Texture.h"
#include "Rectangle.h"
#include "ButtonBGameAddColumn.h"
#include "SceneManager.h"
#include "SceneScore.h"
#include "ButtonBScoreQuit.h"
#include "ButtonBGamePause.h"
#include "Sound.h"
#include "SceneTutorialElf.h"
#include "SceneTutorialEnd.h"
#include "Music.h"

const int game::SceneGame::TIME_TO_SPAWN = 7500; // Milliseconds
const float game::SceneGame::Y_MIN_CANDY_CANE = 400.f;
const float game::SceneGame::Y_MAX_CANDY_CANE = 450.f;
const float game::SceneGame::VELOCITY_CANDY_CANE = 100.f;
const int game::SceneGame::INITIAL_SCORE_TO_NEXT_LEVEL = 1000;
const float game::SceneGame::Y_START_TREE = 540;

game::SceneGame::SceneGame()
{
}

game::SceneGame::~SceneGame()
{
	delete textureBackground;
	textureBackground = nullptr;
	delete textureWall;
	textureWall = nullptr;
	delete presentManager;
	presentManager = nullptr;
	delete timer;
	timer = nullptr;
	delete buttonAddColumn;
	buttonAddColumn = nullptr;
	delete textScore;
	textScore = nullptr;
	delete textTotalScore;
	textTotalScore = nullptr;
	delete textureTree;
	textureTree = nullptr;
	delete textureCandyCane;
	textureCandyCane = nullptr;
	delete textTitleScoreToNextLevel;
	textTitleScoreToNextLevel = nullptr;
	delete textScoreToNextLevel;
	textScoreToNextLevel = nullptr;
	delete buttonAddColumn;
	buttonAddColumn = nullptr;
	delete buttonPause;
	buttonPause = nullptr;
	delete buttonQuit;
	buttonQuit = nullptr;
	delete soundAddColumnWarning;
	soundAddColumnWarning = nullptr;
	delete soundAddColumn;
	soundAddColumn = nullptr;
	delete textLevel;
	textLevel = nullptr;
	delete textLevelNumber;
	textLevelNumber = nullptr;
}

void game::SceneGame::SetState(State state)
{
	Scene::SetState(state);

	// If the given state is not ON pause the timer, else unpause it
	if (state != State::ON)
	{
		if (timer != nullptr)
		{
			PauseTimer();
		}
	}
	else
	{
		UnpauseTimer();
	}
}

int game::SceneGame::GetTotalScore()
{
	return totalScore;
}

void game::SceneGame::SetShowAddColumn(bool showAddColumn)
{
	this->showAddColumn = showAddColumn;

	// If show add column is true
	if (showAddColumn)
	{
		// Play the show add column sound
		soundAddColumnWarning->Play();
		
		// If i'ts the first time the players has no plays, show a warning text
		if (firstElf)
		{
			SceneManager::Insert("tutorialelf", new SceneTutorialElf());
			firstElf = false;
		}
	}
}

int game::SceneGame::GetLevel()
{
	return level;
}

void game::SceneGame::InsertPresentsFirstColumn()
{
	soundAddColumn->Play();

	// If it's the first time a column is added, show a warning
	if (firstPush)
	{
		SceneManager::Insert("tutorialend", new SceneTutorialEnd());
		firstPush = false;
	}

	// Stop the candy cane from moving and rendering
	showAddColumn = false;

	// Restart the timer
	timer->Start();

	// If the last column of presents is not null exit the application (lose the game)
	if (presentManager->IsLastColumnOccupied())
	{
		buttonQuit->enabled = false;
		timer->Stop();
		SceneManager::Delete(this);

		SceneScore* sceneScore = new SceneScore();
		sceneScore->level = level;
		sceneScore->score = totalScore;

		SceneManager::Insert("score", sceneScore);

		sceneScore = nullptr;
		return;
	}

	// Create new presents in the first column
	presentManager->SpawnNewPresents();

	// Check if it exists adjacencies of the same type between the presents
	SetShowAddColumn(!presentManager->ExistsAdjacenciesOfSameType());
}

void game::SceneGame::AddToScore(int score)
{
	int oldLevel = level;
	totalScore += score;
	textTotalScore->SetText(std::to_string(totalScore));
	
	// Advance level while the totalScore is greater or equal to the score to reach the next level
	while (totalScore >= scoreToNextLevel)
	{
		scoreToNextLevel *= 2;
		level++;
	}

	// If the total score is greater than the score needed to advance a level, clear the board and respawn new presents, set a new score to next level, stop the timer and respawn
	if (oldLevel != level)
	{
		textLevelNumber->SetText(std::to_string(level));
		soundAddColumn->Play();
		presentManager->ClearAndRespawn();
		timer->Stop();
		presentsRespawning = true;
		textScoreToNextLevel->SetText(std::to_string(scoreToNextLevel));
	}
}

void game::SceneGame::PauseTimer()
{
	timer->Pause();
}

void game::SceneGame::UnpauseTimer()
{
	timer->Unpause();
}

void game::SceneGame::Initialize()
{
	af::Content* content = af::Engine::GetContent();

	presentManager = new PresentManager(this, 26.f, 60.f);

	textureBackground = new af:: Texture(content->GetTexture("background"));
	textureBackground->SetBlendMode(SDL_BLENDMODE_BLEND);
	textureBackground->SetColor(255, 255, 255, 255);

	textureWall = new af::Texture(content->GetTexture("wall"));
	textureWall->SetBlendMode(SDL_BLENDMODE_BLEND);
	textureWall->SetColor(255, 255, 255, 255);

	af::Texture* textureAddColumnNormal = new af::Texture(content->GetTexture("spriteSheet"), 50, 85, 213, 54, 23, 39);
	textureAddColumnNormal->SetBlendMode(SDL_BLENDMODE_BLEND);
	textureAddColumnNormal->SetColor(255, 255, 255, 255);

	buttonAddColumn = new Button(textureAddColumnNormal);
	buttonAddColumn->SetX(100);
	buttonAddColumn->SetY(500);
	buttonAddColumn->SetRectangle(new af::Rectangle(65, 447, 60, 80));
	buttonAddColumn->SetButtonBehaviour(new ButtonBGameAddColumn(this));

	textScore = new af::Text(content->GetFont("8bit14"));
	textScore->SetColor(255, 255, 255, 0);
	textScore->SetText("Score");

	textTotalScore = new af::Text(content->GetFont("8bit14"));
	textTotalScore->SetColor(255, 255, 255, 0);
	textTotalScore->SetText("0");

	textureTree = new af::Texture(content->GetTexture("spriteSheet"), 90, 106, 0, 106, 109, 128);
	textureTree->SetColor(255, 255, 255, 255);
	textureTree->SetBlendMode(SDL_BLENDMODE_BLEND);

	textureCandyCane = new af::Texture(content->GetTexture("spriteSheet"), 50, 25, 178, 54, 30, 15);
	textureCandyCane->SetColor(255, 255, 255, 255);
	textureCandyCane->SetBlendMode(SDL_BLENDMODE_BLEND);

	textureAddColumnNormal = nullptr;
	showAddColumn = false;
	candyCaneUp = false;
	yCandyCane = Y_MIN_CANDY_CANE;
	scoreToNextLevel = INITIAL_SCORE_TO_NEXT_LEVEL;
	presentsRespawning = true;

	textTitleScoreToNextLevel = new af::Text(content->GetFont("8bit14"));
	textTitleScoreToNextLevel->SetColor(255, 255, 255, 0);
	textTitleScoreToNextLevel->SetText("Next Level Score");

	textScoreToNextLevel = new af::Text(content->GetFont("8bit14"));
	textScoreToNextLevel->SetColor(255, 255, 255, 0);
	textScoreToNextLevel->SetText(std::to_string(scoreToNextLevel));

	af::Text* textQuit = new af::Text(content->GetFont("8bit16"));
	textQuit->SetColor(206, 186, 21, 0);
	textQuit->SetText("Quit");

	buttonQuit = new Button(new af::Texture(content->GetTexture("spriteSheet"), 150, 41, 153, 27, 100, 27));
	buttonQuit->SetX(670);
	buttonQuit->SetY(500);
	buttonQuit->SetRectangle(new af::Rectangle(595, 479, 150, 41));
	buttonQuit->SetPressedTexture(new af::Texture(content->GetTexture("spriteSheet"), 150, 41, 153, 0, 100, 27));
	buttonQuit->SetXText(670);
	buttonQuit->SetYText(500);
	buttonQuit->SetText(textQuit);
	buttonQuit->SetButtonBehaviour(new ButtonBScoreQuit());
	buttonQuit->SetSoundClick(new af::Sound(content->GetSound("clickButton")));

	af::Text* textPause = new af::Text(content->GetFont("8bit16"));
	textPause->SetColor(206, 186, 21, 0);
	textPause->SetText("Pause");

	buttonPause = new Button(new af::Texture(content->GetTexture("spriteSheet"), 150, 41, 153, 27, 100, 27));
	buttonPause->SetX(670);
	buttonPause->SetY(400);
	buttonPause->SetRectangle(new af::Rectangle(595, 379, 150, 41));
	buttonPause->SetPressedTexture(new af::Texture(content->GetTexture("spriteSheet"), 150, 41, 153, 0, 100, 27));
	buttonPause->SetXText(670);
	buttonPause->SetYText(400);
	buttonPause->SetText(textPause);
	buttonPause->SetButtonBehaviour(new ButtonBGamePause(this));
	buttonPause->SetSoundClick(new af::Sound(content->GetSound("clickButton")));

	soundAddColumnWarning = new af::Sound(content->GetSound("addColumnWarning"));
	soundAddColumnWarning->SetVolume(75);

	soundAddColumn = new af::Sound(content->GetSound("addColumn"));
	soundAddColumn->SetVolume(25);

	level = 1;

	textLevel = new af::Text(content->GetFont("8bit16"));
	textLevel->SetColor(255, 255, 255, 0);
	textLevel->SetText("Level");

	textLevelNumber = new af::Text(content->GetFont("8bit16"));
	textLevelNumber->SetColor(255, 255, 255, 0);
	textLevelNumber->SetText(std::to_string(level));

	firstElf = true;
	firstPush = true;

	if (!af::Music::IsPlaying())
	{
		af::Music::SetVolume(20);
		af::Music::Play(content->GetMusic("gameMusic"));
	}

	textPause = nullptr;
	textQuit = nullptr;
	textureAddColumnNormal = nullptr;
	content = nullptr;

	timer = new af::Timer();
}

void game::SceneGame::HandleInput(SDL_Event& e)
{
	if (!presentsRespawning)
	{
		presentManager->HandleInput(e);
		buttonAddColumn->HandleInput(e);
	}

	buttonPause->HandleInput(e);
	buttonQuit->HandleInput(e);
}

void game::SceneGame::Update()
{
	presentManager->Update();

	if (!presentsRespawning)
	{
		// If it's time to spawn more presents
		if (timer->GetTicks() >= 7500)
		{
			InsertPresentsFirstColumn();
		}
	}
	else 
	{
		if (presentManager->CheckIfTransitioning())
		{
			presentsRespawning = false;
			timer->Start();
		}
	}

	// If the player has to add a new column
	if (showAddColumn)
	{
		// If the candy cane is not going up move it down, else move it up
		if (!candyCaneUp)
		{
			yCandyCane += VELOCITY_CANDY_CANE * af::Engine::GetApplication()->GetDeltaTime();

			if (yCandyCane > Y_MAX_CANDY_CANE)
			{
				yCandyCane = Y_MAX_CANDY_CANE;
				candyCaneUp = !candyCaneUp;
			}
		}
		else
		{
			yCandyCane -= VELOCITY_CANDY_CANE * af::Engine::GetApplication()->GetDeltaTime();

			if (yCandyCane < Y_MIN_CANDY_CANE)
			{
				yCandyCane = Y_MIN_CANDY_CANE;
				candyCaneUp = !candyCaneUp;
			}
		}
	}
}

void game::SceneGame::Render(af::Renderer* renderer)
{
	renderer->Draw(textureWall, 254, 503);
	renderer->Draw(textureBackground, 400, 300);
	presentManager->Render(renderer);
	buttonAddColumn->Render(renderer);
	renderer->DrawText(textScore, 670, 14);
	renderer->DrawText(textTotalScore, 670, 34);
	renderer->DrawText(textTitleScoreToNextLevel, 670, 74);
	renderer->DrawText(textScoreToNextLevel, 670, 94);
	renderer->DrawText(textLevel, 670, 144);
	renderer->DrawText(textLevelNumber, 670, 164);
	buttonPause->Render(renderer);
	buttonQuit->Render(renderer);

	if (showAddColumn)
	{
		renderer->Draw(textureCandyCane, 100, static_cast<int>(yCandyCane), -90);
	}

	int treeHeight = timer->GetTicks() * textureTree->GetHeight() / TIME_TO_SPAWN;
	int treeSrcY = treeHeight * textureTree->GetSrcHeight() / textureTree->GetHeight();
	float treeY = round(Y_START_TREE - static_cast<float>(treeHeight) / 2.f);

	renderer->Draw(textureTree, 250, static_cast<int>(treeY), textureTree->GetWidth(), treeHeight, textureTree->GetSrcX(), textureTree->GetSrcY(), textureTree->GetSrcWidth(), treeSrcY, 0);
}