#include "ApplicationConfiguration.h"
#include "Application.h"
#include "Game.h"

int main(int argc, char* args[])
{
	af::ApplicationConfiguration applicationConfiguration;
	applicationConfiguration.title = "XMAS Presents";
	applicationConfiguration.screenWidth = 800;
	applicationConfiguration.screenHeight = 600;

	af::Application application(&applicationConfiguration, &af::Game());

	return 0;
}