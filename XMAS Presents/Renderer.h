#ifndef RENDERER_H
#define RENDERER_H

#include <SDL.h>

namespace af
{
	class Application;
	class Texture;
	class Text;

	class Renderer
	{
		friend class Application;

	private:
		SDL_Renderer* renderer;

	public:
		Renderer(SDL_Renderer* renderer);
		~Renderer();

		void SetClearColor(Uint8 r, Uint8 g, Uint8 b, Uint8 a);

		// Draw a texture in a given position
		void Draw(const Texture* texture, int dstX, int dstY) const;

		// Draw a texture in a given position with rotation
		void Draw(const Texture* texture, int dstX, int dstY, double angle) const;

		// Draw a texture in a given position with full customization
		void Draw(const Texture* texture, int dstX, int dstY, int dstWidth, int dstHeight, int srcX, int srcY, int srcWidth, int srctHeight, double angle) const;

		// Draws text in a position with a given font
		void DrawText(const Text* text, int dstX, int dstY) const;

		void Clear();
		void Present();
	};
}

#endif