#include "PresentDefault.h"

#include <SDL.h>

#include "Texture.h"
#include "Content.h"
#include "Engine.h"
#include "PresentManager.h"

game::PresentDefault::PresentDefault(PresentManager* presentManager, int width, int height)
	:Present(presentManager, width, height)
{
	af::Content* content = af::Engine::GetContent();

	type = rand() % 5 + 1;
	texture = new af::Texture(content->GetTexture("spriteSheet"), width, height, (type - 1) % 3 * 51, (type - 1) / 3 * 53, 51, 53);
	texture->SetColor(255, 255, 255, 255);
	texture->SetBlendMode(SDL_BLENDMODE_BLEND);
	score = 25;

	content = nullptr;
}

game::PresentDefault::~PresentDefault()
{
}