#include "Present.h"

#include <vector>

#include "Rectangle.h"
#include "Texture.h"
#include "Renderer.h"
#include "Content.h"
#include "Engine.h"
#include "Application.h"
#include "PresentManager.h"

const float game::Present::VELOCITY = 200.f;

game::Present::Present(PresentManager* presentManager, int width, int height)
{
	this->presentManager = presentManager;
	type = 0;
	marked = false;
	this->texture = nullptr;
	this->rectangle = new af::Rectangle(0, 0, width, height);
	for (unsigned int i = 0; i < 4; i++)
	{
		adjacentPresents[i] = nullptr;
	}
	transitioning = false;
	xTarget = 0;
	yTarget = 0;

	af::Content* content = af::Engine::GetContent();

	textureHover = new af::Texture(content->GetTexture("spriteSheet"), 53, 55, 215, 187, 41, 43);
	textureHover->SetBlendMode(SDL_BLENDMODE_BLEND);
	textureHover->SetColor(255, 255, 255, 255);
	hovered = false;
}

game::Present::~Present()
{
	delete texture;
	delete rectangle;

	texture = nullptr;
	rectangle = nullptr;

	for (unsigned int i = 0; i < 4; i++)
	{
		adjacentPresents[i] = nullptr;
	}
}

bool game::Present::IsMarked()
{
	return marked;
}

void game::Present::SetMarked(bool marked)
{
	this->marked = marked;
}

float game::Present::GetX()
{
	return x;
}

float game::Present::GetY()
{
	return y;
}

void game::Present::SetPosition(float x, float y)
{
	this->x = x;
	this->y = y;
	rectangle->x = static_cast<int>(x) - rectangle->width / 2;
	rectangle->y = static_cast<int>(y) - rectangle->height / 2;
}

const af::Rectangle* game::Present::GetRectangle()
{
	return rectangle;
}

bool game::Present::IsTransitioning()
{
	return transitioning;
}

void game::Present::SetTargetX(float x)
{
	xTarget = x;
}

void game::Present::SetTargetY(float y)
{
	yTarget = y;
}

int game::Present::GetScore()
{
	return score;
}

int game::Present::GetNAdjacencies()
{
	int nAdjancencies = 0;

	for (unsigned int i = 0; i < 4; i++)
	{
		if (adjacentPresents[i] != nullptr)
		{
			nAdjancencies++;
		}
	}

	return nAdjancencies;
}

void game::Present::MoveToTarget()
{
	transitioning = true;
}

void game::Present::InsertAdjacentPresent(Present* present)
{
	// Check if the present already exists
	for (unsigned int i = 0; i < 4; i++)
	{
		if (adjacentPresents[i] != nullptr)
		{
			if (adjacentPresents[i] == present)
			{
				return;
			}
		}
	}
	
	// Add present in an empty slot if availiable and add itself to the adjacent present
	for (unsigned int i = 0; i < 4; i++)
	{
		if (adjacentPresents[i] == nullptr)
		{
			adjacentPresents[i] = present;
			present->InsertAdjacentPresent(this);

			break;
		}
	}
}

void game::Present::ClearAdjacentPresents()
{
	for (unsigned int i = 0; i < 4; i++)
	{
		adjacentPresents[i] = nullptr;
	}
}

bool game::Present::HasAdjacentOfSameType()
{
	// Check if the present has adjacent presents of the same type
	for (unsigned int i = 0; i < 4; i++)
	{
		if (adjacentPresents[i] != nullptr)
		{
			if (adjacentPresents[i]->type == type)
			{
				return true;
			}
		}
	}

	return false;
}

void game::Present::GetPresentsOfType(int type, std::vector<Present*>& presents)
{
	presents.push_back(this);

	// Loop through the adjacent presents
	for (unsigned int i = 0; i < 4; i++)
	{
		// If the present is not a null pointer
		if (adjacentPresents[i] != nullptr)
		{
			bool exists = false;

			// Check if the present already exists in the presents list
			for (unsigned int j = 0; j < presents.size(); j++)
			{
				if (presents[j] == adjacentPresents[i])
				{
					exists = true;
					break;
				}
			}

			// If the present doesn't exist in the list and is of the same type, save it in the list and get it's adjacents
			if (!exists && adjacentPresents[i]->type == type)
			{
				presents.push_back(adjacentPresents[i]);
				adjacentPresents[i]->GetPresentsOfType(type, presents);
			}
		}
	}
}

void game::Present::MarkForDeletion()
{
	// Create a list to save all the adjacent presents and add itself
	std::vector<Present*> presents;

	// Get all adjacent presents of the same type
	GetPresentsOfType(type, presents);

	for (Present* present : presents)
	{
		present->marked = true;
	}
}

void game::Present::HandleInput(SDL_Event& e)
{
	// If the mouse moved, check if it intersects with the present and check hovered
	if (e.type == SDL_MOUSEMOTION)
	{
		af::Rectangle mouseRectangle(e.motion.x, e.motion.y, 1, 1);

		if (mouseRectangle.Intersects(rectangle))
		{
			hovered = true;
		}
		else
		{
			hovered = false;
		}
	}
}

void game::Present::Update()
{
	if (transitioning)
	{
		bool reachedXTarget = false;
		bool reachedYTarget = false;
		float velocity = VELOCITY * af::Engine::GetApplication()->GetDeltaTime();

		if (xTarget == x)
		{
			reachedXTarget = true;
		}
		else if (xTarget < x)
		{
			x -= velocity;

			if (xTarget > x)
			{
				x = xTarget;
			}
		}
		else
		{
			x += velocity;

			if (xTarget < x)
			{
				x = xTarget;
			}
		}

		if (yTarget == y)
		{
			reachedYTarget = true;
		}
		else if (yTarget < y)
		{
			y -= velocity;

			if (yTarget > y)
			{
				y = yTarget;
			}
		}
		else
		{
			y += velocity;

			if (yTarget < y)
			{
				y = yTarget;
			}
		}

		rectangle->x = static_cast<int>(x) - rectangle->width / 2;
		rectangle->y = static_cast<int>(y) - rectangle->height / 2;

		if (reachedXTarget && reachedYTarget)
		{
			transitioning = false;
		}
	}
}

void game::Present::Render(af::Renderer* renderer)
{
	renderer->Draw(texture, static_cast<int>(x), static_cast<int>(y));

	if (hovered)
	{
		renderer->Draw(textureHover, static_cast<int>(x), static_cast<int>(y));
	}
}

void game::Present::DeleteAdjacentPresent(Present* present)
{
	for (unsigned int i = 0; i < 4; i++)
	{
		if (adjacentPresents[i] != nullptr)
		{
			if (adjacentPresents[i] == present)
			{
				adjacentPresents[i] = nullptr;
				break;
			}
		}
	}
}