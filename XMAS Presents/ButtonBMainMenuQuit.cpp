#include "ButtonBMainMenuQuit.h"

#include "Engine.h"
#include "Application.h"

game::ButtonBMainMenuQuit::ButtonBMainMenuQuit()
{
}

game::ButtonBMainMenuQuit::~ButtonBMainMenuQuit()
{
}

void game::ButtonBMainMenuQuit::OnClick()
{
	af::Engine::GetApplication()->Quit();
}