#include "SceneTutorialEnd.h"

#include "Renderer.h"
#include "Content.h"
#include "Engine.h"
#include "Texture.h"
#include "Text.h"
#include "Timer.h"
#include "SceneManager.h"

const int game::SceneTutorialEnd::MAX_TIME = 3000;

game::SceneTutorialEnd::SceneTutorialEnd()
{
}

game::SceneTutorialEnd::~SceneTutorialEnd()
{
}

void game::SceneTutorialEnd::Initialize()
{
	af::Content* content = af::Engine::GetContent();

	textureBalloon = new af::Texture(content->GetTexture("spriteSheet"), 400, 100, 138, 105, 50, 50);
	textureBalloon->SetBlendMode(SDL_BLENDMODE_BLEND);
	textureBalloon->SetColor(255, 255, 255, 255);

	textBalloon1 = new af::Text(content->GetFont("8bit12"));
	textBalloon1->SetColor(0, 0, 0, 0);
	textBalloon1->SetText("Dont let the presents reach");

	textBalloon2 = new af::Text(content->GetFont("8bit12"));
	textBalloon2->SetColor(0, 0, 0, 0);
	textBalloon2->SetText("the end of the mountain");

	content = nullptr;

	timer = new af::Timer();
	timer->Start();
}

void game::SceneTutorialEnd::HandleInput(SDL_Event& e)
{
}

void game::SceneTutorialEnd::Update()
{
	if (timer->GetTicks() > MAX_TIME)
	{
		SceneManager::Delete(this);
	}
}

void game::SceneTutorialEnd::Render(af::Renderer* renderer)
{
	renderer->Draw(textureBalloon, 270, 445);
	renderer->DrawText(textBalloon1, 290, 430);
	renderer->DrawText(textBalloon2, 290, 450);
}