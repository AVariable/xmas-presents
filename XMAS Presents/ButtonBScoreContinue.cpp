#include "ButtonBScoreContinue.h"

#include "SceneManager.h"
#include "SceneGame.h"

game::ButtonBScoreContinue::ButtonBScoreContinue()
{
}

game::ButtonBScoreContinue::~ButtonBScoreContinue()
{
}

void game::ButtonBScoreContinue::OnClick()
{
	SceneManager::DeleteAll();
	SceneManager::Insert("game", new SceneGame());
}