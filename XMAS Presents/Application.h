#ifndef APPLICATION_H
#define APPLICATION_H

#include <vector>

namespace af
{
	class Timer;
	class ApplicationConfiguration;
	class Game;
	class Renderer;

	class Application
	{
	private:
		bool running;
		af::Timer* fpsTimer;
		af::Timer* capTimer;
		af::Timer* deltaTimer;
		af::Timer* stepTimer;
		int countedFrames;
		int fps;
		int tickPerFrame;

	public:
		Application(af::ApplicationConfiguration* applicationConfiguration, af::Game* game);
		~Application();

		bool IsRunning();
		float GetDeltaTime();
		void SetFPS(int fps);

		// Sets the running variable to false so that the application can quit normally
		void Quit();

	private:
		// Initializes all SDL subsystems
		bool InitializeSDL();

		// Initializes the window of the application with the given configurations
		bool InitializeWindow(af::ApplicationConfiguration* applicationConfiguration);

		// Initializes a renderer to draw objects
		bool InitializeRenderer();

		// Initializes all systems
		bool Initialize(af::ApplicationConfiguration* applicationConfiguration);
		
		// Disposes all the variables used in the application
		void Dispose();
	};
}

#endif