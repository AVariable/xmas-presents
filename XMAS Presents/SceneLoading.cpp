#include "SceneLoading.h"

#include <SDL.h>
#include <iostream>

#include "Texture.h"
#include "Text.h"
#include "Engine.h"
#include "Content.h"
#include "Timer.h"
#include "SceneManager.h"
#include "SceneMainMenu.h"
#include "Renderer.h"

game::SceneLoading::SceneLoading()
{
}

game::SceneLoading::~SceneLoading()
{
	delete texture;
	delete textLoading;
	delete timer;

	texture = nullptr;
	textLoading = nullptr;
	timer = nullptr;
}

void game::SceneLoading::Initialize()
{
	af::Content* content = af::Engine::GetContent();

	content->LoadTexture("Resources/spriteSheet.png", "spriteSheet");
	content->LoadTexture("Resources/bg.png", "background");
	content->LoadTexture("Resources/wall.png", "wall");
	content->LoadFont("Resources/8-BIT.ttf", "8bit36", 36);
	content->LoadFont("Resources/8-BIT.ttf", "8bit16", 16);
	content->LoadFont("Resources/8-BIT.ttf", "8bit12", 12);
	content->LoadFont("Resources/8-BIT.ttf", "8bit14", 14);
	content->LoadTexture("Resources/paused.png", "pausedBG");
	content->LoadSound("Resources/add_column_warning.wav", "addColumnWarning");
	content->LoadSound("Resources/click_button.wav", "clickButton");
	content->LoadSound("Resources/click_present.wav", "clickPresent");
	content->LoadSound("Resources/add_column.wav", "addColumn");
	content->LoadMusic("Resources/main_menu_music.mp3", "mainMenuMusic");
	content->LoadMusic("Resources/game_music.mp3", "gameMusic");

	texture = new af::Texture(content->GetTexture("spriteSheet"), 256, 301, 0, 106, 109, 128);
	texture->SetColor(255, 255, 255, 255);
	texture->SetBlendMode(SDL_BLENDMODE_BLEND);

	textLoading = new af::Text(content->GetFont("8bit36"));
	textLoading->SetText("LOADING");
	textLoading->SetColor(253, 233, 72, 255);

	timer = new af::Timer();
	timer->Start();
}

void game::SceneLoading::HandleInput(SDL_Event& e)
{
}

void game::SceneLoading::Update()
{
	if (timer->GetTicks() >= 2000)
	{
		SceneManager::Delete(this);
		SceneManager::Insert("mainmenu", new SceneMainMenu());
	}
}

void game::SceneLoading::Render(af::Renderer* renderer)
{
	renderer->SetClearColor(95, 197, 212, 0);
	renderer->Draw(texture, 400, 300);
	renderer->DrawText(textLoading, 400, 500);
}