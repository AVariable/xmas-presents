#ifndef BUTTON_BEHAVIOUR_MAINMENU_PLAY_H
#define BUTTON_BEHAVIOUR_MAINMENU_PLAY_H

#include "ButtonBehaviour.h"

namespace game
{
	class ButtonBMainMenuPlay : virtual public ButtonBehaviour
	{
	public:
		ButtonBMainMenuPlay();
		~ButtonBMainMenuPlay();

		void OnClick();
	};
}

#endif