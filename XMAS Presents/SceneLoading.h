#ifndef SCENE_LOADING_H
#define SCENE_LOADING_H

#include "Scene.h"

namespace af
{
	class Texture;
	class Text;
	class Timer;
	class Renderer;
}

namespace game
{
	class SceneLoading : public Scene
	{
	private:
		af::Texture* texture;
		af::Text* textLoading;
		af::Timer* timer;

	public:
		SceneLoading();
		~SceneLoading();

	protected:
		void Initialize();
		void HandleInput(SDL_Event& e);
		void Update();
		void Render(af::Renderer* renderer);
	};
}

#endif