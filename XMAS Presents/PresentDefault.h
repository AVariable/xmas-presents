#ifndef PRESENT_DEFAULT_H
#define PRESENT_DEFAULT_H

#include "Present.h"

namespace af
{
	class Texture;
}

namespace game
{
	class PresentManager;

	class PresentDefault : public Present
	{
	public:
		PresentDefault(PresentManager* presentManager, int width, int height);
		~PresentDefault();
	};
}

#endif