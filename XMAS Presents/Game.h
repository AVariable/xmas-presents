#ifndef GAME_H
#define GAME_H

#include <SDL.h>

namespace af
{
	class Renderer;

	class Game
	{
	public:
		Game();
		~Game();

		// Initializes all game content
		void Initialize();

		// Disposes all game content
		void Dispose();

		// Handles the input
		void HandleInput(SDL_Event& e);

		// Updates game logic
		void Update();

		// Renders the game on to the screen
		void Render(Renderer* renderer);
	};
}

#endif