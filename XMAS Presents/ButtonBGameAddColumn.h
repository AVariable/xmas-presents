#ifndef BUTTON_BEHAVIOUR_GAME_ADD_COLUMN_H
#define BUTTON_BEHAVIOUR_GAME_ADD_COLUMN_H

#include "ButtonBehaviour.h"

namespace game
{
	class SceneGame;

	class ButtonBGameAddColumn : virtual public ButtonBehaviour
	{
	private:
		SceneGame* sceneGame;

	public:
		ButtonBGameAddColumn(SceneGame* sceneGame);
		~ButtonBGameAddColumn();

		void OnClick();
	};
}

#endif