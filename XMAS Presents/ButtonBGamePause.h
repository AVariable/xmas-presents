#ifndef BUTTON_BEHAVIOUR_GAME_PAUSE_H
#define BUTTON_BEHAVIOUR_GAME_PAUSE_H

#include "ButtonBehaviour.h"

namespace game
{
	class SceneGame;

	class ButtonBGamePause : public ButtonBehaviour
	{
	private:
		SceneGame* sceneGame;

	public:
		ButtonBGamePause(SceneGame* sceneGame);
		~ButtonBGamePause();

		void OnClick();
	};
}

#endif