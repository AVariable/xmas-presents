#ifndef APPLICATION_CONFIGURATION_H
#define APPLICATION_CONFIGURATION_H

#include <string>

namespace af
{
	class ApplicationConfiguration
	{
	public:
		std::string title;
		int screenWidth;
		int screenHeight;
		int fps;

		ApplicationConfiguration();
		~ApplicationConfiguration();
	};
}

#endif