#include "Window.h"

#include <SDL.h>

af::Window::Window(SDL_Window* window)
{
	this->window = window;
}

af::Window::~Window()
{
	SDL_DestroyWindow(window);
	window = nullptr;
}