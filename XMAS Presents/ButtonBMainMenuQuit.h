#ifndef BUTTON_BEHAVIOUR_MAINMENU_QUIT_H
#define BUTTON_BEHAVIOUR_MAINMENU_QUIT_H

#include "ButtonBehaviour.h"

namespace game
{
	class ButtonBMainMenuQuit : virtual public ButtonBehaviour
	{
	public:
		ButtonBMainMenuQuit();
		~ButtonBMainMenuQuit();

		void OnClick();
	};
}

#endif