#ifndef WINDOW_H
#define WINDOW_H

struct SDL_Window;

namespace af
{
	class Window
	{
		friend class Application;

	private:
		SDL_Window* window;

	public:
		Window(SDL_Window* window);
		~Window();
	};
}

#endif