#include "SceneTutorialStart.h"

#include "Renderer.h"
#include "Content.h"
#include "Engine.h"
#include "Text.h"
#include "SceneGame.h"
#include "SceneManager.h"

game::SceneTutorialStart::SceneTutorialStart(SceneGame* sceneGame)
{
	this->sceneGame = sceneGame;
}

game::SceneTutorialStart::~SceneTutorialStart()
{
	delete textTitle;
	textTitle = nullptr;
	delete textClick;
	textClick = nullptr;
}

void game::SceneTutorialStart::Initialize()
{
	af::Content* content = af::Engine::GetContent();

	textTitle = new af::Text(content->GetFont("8bit14"));
	textTitle->SetText("Match 2 or more presents to steal them");
	textTitle->SetColor(255, 255, 255, 255);

	textClick = new af::Text(content->GetFont("8bit12"));
	textClick->SetText("Click to continue");
	textClick->SetColor(255, 255, 255, 255);

	content = nullptr;
}

void game::SceneTutorialStart::HandleInput(SDL_Event& e)
{
	if (e.type == SDL_MOUSEBUTTONDOWN)
	{
		SceneManager::Delete(this);
		sceneGame->SetState(Scene::State::ON);
	}
}

void game::SceneTutorialStart::Update()
{
}

void game::SceneTutorialStart::Render(af::Renderer* renderer)
{
	renderer->DrawText(textTitle, 275, 150);
	renderer->DrawText(textClick, 275, 175);
}