#include "Sound.h"

af::Sound::Sound(Mix_Chunk* sound)
{
	this->sound = sound;
	volume = MIX_MAX_VOLUME;
}

af::Sound::~Sound()
{
	sound = nullptr;
}

int af::Sound::GetVolume()
{
	return volume;
}

void af::Sound::SetVolume(int volume)
{
	// Sets the volume to a value between 0 and 128
	this->volume = volume < 0 ? 0 : volume > MIX_MAX_VOLUME ? MIX_MAX_VOLUME : volume;
}

void af::Sound::Play()
{
	int channel = Mix_PlayChannel(-1, sound, 0);
	Mix_Volume(channel, volume);
}