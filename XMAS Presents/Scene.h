#ifndef SCENE_H
#define SCENE_H

#include <SDL.h>
#include <string>

namespace af
{
	class Renderer;
}

namespace game
{
	class Scene
	{
		friend class SceneManager;

	public:
		enum State { ON, FROZEN, OFF };

	private:
		State state;

	public:
		Scene();
		~Scene();

		State GetState();
		virtual void SetState(State state);

	protected:
		virtual void Initialize() = 0;
		virtual void HandleInput(SDL_Event& e) = 0;
		virtual void Update() = 0;
		virtual void Render(af::Renderer* renderer) = 0;
	};
}

#endif