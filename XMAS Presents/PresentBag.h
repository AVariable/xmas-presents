#ifndef PRESENT_BAG_H
#define PRESENT_BAG_H

#include <vector>

#include "Present.h"

namespace game
{
	class PresentManager;

	class PresentBag : public Present
	{
	public:
		PresentBag(PresentManager* presentManager, int width, int height);
		~PresentBag();

		void GetPresentsOfType(int type, std::vector<Present*>& presents);
	};
}

#endif