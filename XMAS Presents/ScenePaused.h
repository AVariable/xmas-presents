#ifndef SCENE_PAUSED_H
#define SCENE_PAUSED_H

#include <SDL.h>

#include "Scene.h"

namespace af
{
	class Texture;
	class Renderer;
}

namespace game
{
	class SceneGame;

	class ScenePaused : public Scene
	{
	private:
		SceneGame* sceneGame;
		af::Texture* textureBG;

	public:
		ScenePaused(SceneGame* sceneGame);
		~ScenePaused();

		void Initialize();
		void HandleInput(SDL_Event& e);
		void Update();
		void Render(af::Renderer* renderer);
	};
}

#endif