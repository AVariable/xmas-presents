#include "SceneMainMenu.h"

#include <SDL.h>
#include <iostream>

#include "Button.h"
#include "Content.h"
#include "Text.h"
#include "Engine.h"
#include "Texture.h"
#include "Rectangle.h"
#include "Application.h"
#include "SceneManager.h"
#include "SceneGame.h"
#include "ButtonBMainMenuQuit.h"
#include "ButtonBMainMenuPlay.h"
#include "Renderer.h"
#include "Sound.h"
#include "Music.h"

game::SceneMainMenu::SceneMainMenu()
{
}

game::SceneMainMenu::~SceneMainMenu()
{
	delete textureBackground;
	textureBackground = nullptr;
	delete textureSanta;
	textureSanta = nullptr;
	delete textureRaindears;
	textureRaindears = nullptr;
	delete textureStar;
	textureStar = nullptr;
	delete textTitle;
	textTitle = nullptr;
	delete buttonQuit;
	buttonQuit = nullptr;
	delete buttonPlay;
	buttonPlay = nullptr;
}

void game::SceneMainMenu::Initialize()
{
	af::Content* content = af::Engine::GetContent();

	textureBackground = new af::Texture(content->GetTexture("background"));
	textureBackground->SetBlendMode(SDL_BLENDMODE_BLEND);
	textureBackground->SetColor(255, 255, 255, 255);

	textureSanta = new af::Texture(content->GetTexture("spriteSheet"), 100, 79, 102, 55, 63, 50);
	textureSanta->SetBlendMode(SDL_BLENDMODE_BLEND);
	textureSanta->SetColor(255, 255, 255, 255);

	textureRaindears = new af::Texture(content->GetTexture("spriteSheet"), 150, 89, 109, 156, 101, 60);
	textureRaindears->SetBlendMode(SDL_BLENDMODE_BLEND);
	textureRaindears->SetColor(255, 255, 255, 255);

	textureStar = new af::Texture(content->GetTexture("spriteSheet"), 19, 18, 236, 55, 19, 18);
	textureStar->SetBlendMode(SDL_BLENDMODE_BLEND);
	textureStar->SetColor(255, 255, 255, 255);

	texturePresent1 = new af::Texture(content->GetTexture("spriteSheet"), 25, 26, 0, 0, 51, 53);
	texturePresent1->SetBlendMode(SDL_BLENDMODE_BLEND);
	texturePresent1->SetColor(255, 255, 255, 255);

	texturePresent2 = new af::Texture(content->GetTexture("spriteSheet"), 25, 26, 51, 0, 51, 53);
	texturePresent2->SetBlendMode(SDL_BLENDMODE_BLEND);
	texturePresent2->SetColor(255, 255, 255, 255);

	textureWall = new af::Texture(content->GetTexture("wall"));
	textureWall->SetBlendMode(SDL_BLENDMODE_BLEND);
	textureWall->SetColor(255, 255, 255, 255);

	textTitle = new af::Text(content->GetFont("8bit36"));
	textTitle->SetText("XMAS PRESENTS");
	textTitle->SetColor(214, 85, 43, 255);
	yTitle = -140;

	af::Text* textQuit = new af::Text(content->GetFont("8bit16"));
	textQuit->SetColor(206, 186, 21, 0);
	textQuit->SetText("Quit");

	af::Texture* btnNormalTexture = new af::Texture(content->GetTexture("spriteSheet"), 150, 41, 153, 27, 100, 27);
	af::Texture* btnPressedTexture = new af::Texture(content->GetTexture("spriteSheet"), 150, 41, 153, 0, 100, 27);

	buttonQuit = new Button(btnNormalTexture);
	buttonQuit->SetX(-100);
	buttonQuit->SetY(200);
	buttonQuit->SetRectangle(new af::Rectangle(25, 179, 150, 41));
	buttonQuit->SetPressedTexture(btnPressedTexture);
	buttonQuit->SetXText(-100);
	buttonQuit->SetYText(200);
	buttonQuit->SetText(textQuit);
	buttonQuit->SetButtonBehaviour(new ButtonBMainMenuQuit());
	buttonQuit->SetSoundClick(new af::Sound(content->GetSound("clickButton")));

	af::Text* textPlay = new af::Text(content->GetFont("8bit16"));
	textPlay->SetColor(206, 186, 21, 0);
	textPlay->SetText("Play");

	buttonPlay = new Button(btnNormalTexture);
	buttonPlay->SetX(-100);
	buttonPlay->SetY(100);
	buttonPlay->SetRectangle(new af::Rectangle(25, 79, 150, 41));
	buttonPlay->SetPressedTexture(btnPressedTexture);
	buttonPlay->SetXText(-100);
	buttonPlay->SetYText(100);
	buttonPlay->SetText(textPlay);
	buttonPlay->SetButtonBehaviour(new ButtonBMainMenuPlay());
	buttonPlay->SetSoundClick(new af::Sound(content->GetSound("clickButton")));

	af::Music::SetVolume(24);
	af::Music::Play(content->GetMusic("mainMenuMusic"));

	transitioning = true;
	btnNormalTexture = nullptr;
	btnPressedTexture = nullptr;
}

void game::SceneMainMenu::HandleInput(SDL_Event& e)
{
	if (!transitioning)
	{
		buttonQuit->HandleInput(e);
		buttonPlay->HandleInput(e);
	}
}

void game::SceneMainMenu::Update()
{
	if (transitioning)
	{
		buttonPlay->SetX(buttonPlay->GetX() + 400.f * af::Engine::GetApplication()->GetDeltaTime());
		buttonPlay->SetXText(buttonPlay->GetX());
		buttonQuit->SetX(buttonPlay->GetX());
		buttonQuit->SetXText(buttonPlay->GetX());
		yTitle += 400.f * af::Engine::GetApplication()->GetDeltaTime();

		if (buttonPlay->GetX() > 100)
		{
			buttonPlay->SetX(100);
			buttonPlay->SetXText(100);
			buttonQuit->SetX(100);
			buttonQuit->SetXText(100);
			yTitle = 60;
			transitioning = false;
		}
	}
}

void game::SceneMainMenu::Render(af::Renderer* renderer)
{
	renderer->Draw(textureWall, 254, 503);
	renderer->Draw(textureBackground, 400, 300);
	renderer->Draw(texturePresent2, 520, 375, -5);
	renderer->Draw(texturePresent2, 500, 395, -15);
	renderer->Draw(texturePresent1, 520, 390, -20);
	renderer->Draw(texturePresent1, 500, 375, 2);
	renderer->Draw(texturePresent1, 540, 375, 30);
	renderer->Draw(textureSanta, 500, 380, -15);
	renderer->Draw(textureRaindears, 380, 410, -15);
	renderer->Draw(textureStar, 758, 25, -147);
	renderer->Draw(textureStar, 600, 158, 40);
	renderer->Draw(textureStar, 485, 20, 157);
	renderer->Draw(textureStar, 389, 158, 68);
	renderer->Draw(textureStar, 300, 24, 57);
	renderer->Draw(textureStar, 50, 50, -5);
	buttonQuit->Render(renderer);
	buttonPlay->Render(renderer);
	renderer->DrawText(textTitle, 500, static_cast<int>(yTitle));
}