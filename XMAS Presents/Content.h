#ifndef CONTENT_H
#define CONTENT_H

#include <map>
#include <string>
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <SDL_mixer.h>

namespace af
{
	class Content
	{
		friend class Application;

	private:
		SDL_Renderer* renderer;
		std::map<std::string, SDL_Texture*> textures;
		std::map<std::string, TTF_Font*> fonts;
		std::map<std::string, Mix_Chunk*> sounds;
		std::map<std::string, Mix_Music*> music;

	public:
		Content(SDL_Renderer* renderer);
		~Content();

		SDL_Texture* GetTexture(std::string name);
		TTF_Font* GetFont(std::string name);
		Mix_Chunk* GetSound(std::string name);
		Mix_Music* GetMusic(std::string name);

		void LoadTexture(std::string path, std::string name);
		void LoadFont(std::string path, std::string name, int size);
		void LoadSound(std::string path, std::string name);
		void LoadMusic(std::string path, std::string name);
	};
}

#endif