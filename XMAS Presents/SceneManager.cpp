#include "SceneManager.h"

#include "Renderer.h"

std::map<std::string, game::Scene*> game::SceneManager::scenesToInsert;
std::map<std::string, game::Scene*> game::SceneManager::scenes;
std::vector<std::string> game::SceneManager::scenesToDelete;

void game::SceneManager::SetStateAll(Scene::State state)
{
	for (std::pair<std::string, Scene*> pair : scenes)
	{
		pair.second->SetState(state);
	}
}

game::Scene* game::SceneManager::Get(std::string name)
{
	// Search for a scene with the same name
	auto iterator = scenes.find(name);

	// Check if the scene exists and return it
	if (iterator != scenes.end())
	{
		return iterator->second;
	}

	return nullptr;
}

void game::SceneManager::Insert(std::string name, Scene* scene)
{
	scenesToInsert.insert(std::make_pair(name, scene));
}

void game::SceneManager::Delete(std::string name)
{
	// Search for a scene with the same name
	auto iterator = scenes.find(name);

	// Check if the scene exists so it can be inserted in the scenesToDelete
	if (iterator != scenes.end())
	{
		scenesToDelete.push_back(name);
	}
}

void game::SceneManager::Delete(Scene* scene)
{
	// Find the scene that is equal to the given scene and add it to the deletion list
	for (std::pair<std::string, Scene*> pair : scenes)
	{
		if (scene == pair.second)
		{
			scenesToDelete.push_back(pair.first);
			break;
		}
	}
}

void game::SceneManager::DeleteAll()
{
	// Delete all scenes
	for (std::pair<std::string, Scene*> pair : scenes)
	{
		scenesToDelete.push_back(pair.first);
	}
}

void game::SceneManager::HandleInput(SDL_Event& e)
{
	// Handle input of scenes
	for (std::pair<std::string, Scene*> pair : scenes)
	{
		if (pair.second->GetState() == Scene::State::ON)
		{
			pair.second->HandleInput(e);
		}
	}
}

void game::SceneManager::Update()
{
	// Delete pending scenes
	for (std::string key : scenesToDelete)
	{
		auto iterator = scenes.find(key);

		// Check if the scene exists so it can be deleted
		if (iterator != scenes.end())
		{
			delete iterator->second;
			iterator->second = nullptr;
			scenes.erase(key);
		}
	}
	scenesToDelete.clear();

	// Insert pending scenes
	for (std::pair<std::string, Scene*> pair : scenesToInsert)
	{
		scenes.insert(pair);
		pair.second->Initialize();
	}
	scenesToInsert.clear();

	// Update logic of scenes only if their state is equal to ON
	for (std::pair<std::string, Scene*> pair : scenes)
	{
		if (pair.second->GetState() == Scene::State::ON)
		{
			pair.second->Update();
		}
	}
}

void game::SceneManager::Render(af::Renderer* renderer)
{
	// Render scenes only if their state is different than Off
	for (std::pair<std::string, Scene*> pair : scenes)
	{
		if (pair.second->GetState() != Scene::State::OFF)
		{
			pair.second->Render(renderer);
		}
	}
}