#ifndef BUTTON_BEHAVIOUR_SCORE_CONTINUE_H
#define BUTTON_BEHAVIOUR_SCORE_CONTINUE_H

#include "ButtonBehaviour.h"

namespace game
{
	class ButtonBScoreContinue : virtual public ButtonBehaviour
	{
	public:
		ButtonBScoreContinue();
		~ButtonBScoreContinue();

		void OnClick();
	};
}

#endif