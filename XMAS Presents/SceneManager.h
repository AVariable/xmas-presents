#ifndef SCENE_MANAGER_H
#define SCENE_MANAGER_H

#include <SDL.h>
#include <map>
#include <string>
#include <vector>

#include "Scene.h"

namespace af
{
	class Renderer;
}

namespace game
{
	class SceneManager
	{
	private:
		static std::map<std::string, Scene*> scenesToInsert;
		static std::map<std::string, Scene*> scenes;
		static std::vector<std::string> scenesToDelete;

	public:
		static Scene* Get(std::string name);

		static void SetStateAll(Scene::State state);

		static void Insert(std::string name, Scene* scene);
		static void Delete(std::string name);
		static void Delete(Scene* scene);
		static void DeleteAll();

		static void HandleInput(SDL_Event& e);
		static void Update();
		static void Render(af::Renderer* renderer);
	};
}

#endif