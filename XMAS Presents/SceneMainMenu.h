#ifndef SCENE_MAINMENU_H
#define SCENE_MAINMENU_H

#include "Scene.h"

namespace af
{
	class Texture;
	class Text;
	class Rectangle;
}

namespace game
{
	class Button;

	class SceneMainMenu : public Scene
	{
	private:
		af::Texture* textureBackground;
		af::Texture* textureSanta;
		af::Texture* textureRaindears;
		af::Texture* textureStar;
		af::Texture* texturePresent1;
		af::Texture* texturePresent2;
		af::Texture* textureWall;
		af::Text* textTitle;
		Button* buttonQuit;
		Button* buttonPlay;
		bool transitioning;
		float yTitle;

	public:
		SceneMainMenu();
		~SceneMainMenu();

	protected:
		void Initialize();
		void HandleInput(SDL_Event& e);
		void Update();
		void Render(af::Renderer* renderer);
	};
}

#endif