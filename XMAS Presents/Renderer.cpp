#include "Renderer.h"

#include <iostream>
#include <SDL_ttf.h>

#include "Application.h"
#include "Texture.h"
#include "Text.h"

af::Renderer::Renderer(SDL_Renderer* renderer)
{
	this->renderer = renderer;
}

af::Renderer::~Renderer()
{
	SDL_DestroyRenderer(renderer);
	renderer = nullptr;
}

void af::Renderer::SetClearColor(Uint8 r, Uint8 g, Uint8 b, Uint8 a)
{
	SDL_SetRenderDrawColor(renderer, r, g, b, a);
}

void af::Renderer::Draw(const Texture* texture, int dstX, int dstY) const
{
	Draw(texture, dstX, dstY, texture->width, texture->height, texture->srcX, texture->srcY, texture->srcWidth, texture->srcHeight, 0);
}

void af::Renderer::Draw(const Texture* texture, int dstX, int dstY, double angle) const
{
	Draw(texture, dstX, dstY, texture->width, texture->height, texture->srcX, texture->srcY, texture->srcWidth, texture->srcHeight, angle);
}

void af::Renderer::Draw(const Texture* texture, int dstX, int dstY, int dstWidth, int dstHeight, int srcX, int srcY, int srcWidth, int srcHeight, double angle) const
{
	SDL_Rect srcRect;

	srcRect.x = srcX;
	srcRect.y = srcY;
	srcRect.w = srcWidth;
	srcRect.h = srcHeight;

	SDL_Rect dstRect;

	dstRect.x = dstX - dstWidth / 2;
	dstRect.y = dstY - dstHeight / 2;
	dstRect.w = dstWidth;
	dstRect.h = dstHeight;

	SDL_Point centerPoint = { dstWidth / 2, dstHeight / 2 };

	SDL_SetTextureColorMod(texture->texture, texture->red, texture->green, texture->blue);
	SDL_SetTextureAlphaMod(texture->texture, texture->alpha);
	SDL_SetTextureBlendMode(texture->texture, texture->blendMode);

	// Render texture to screen
	SDL_RenderCopyEx(renderer, texture->texture, &srcRect, &dstRect, angle, &centerPoint, SDL_FLIP_NONE);
}

void af::Renderer::DrawText(const Text* text, int dstX, int dstY) const
{
	SDL_Color color;
	color.r = text->red;
	color.g = text->green;
	color.b = text->blue;
	color.a = text->alpha;

	// Render text surface
	SDL_Surface* textSurface = TTF_RenderText_Blended(text->font, text->text.c_str(), color);
	if (textSurface == nullptr)
	{
		std::cout << "Unable to render text surface! SDL_ttf Error: " << TTF_GetError() << std::endl;
	}
	else
	{
		// Create texture from surface pixels
		SDL_Texture* texture = SDL_CreateTextureFromSurface(renderer, textSurface);
		if (texture == nullptr)
		{
			std::cout << "Unable to create texture from rendered text! SDL Error: " << SDL_GetError() << std::endl;
		}
		else
		{
			SDL_Rect dstRect;

			dstRect.x = dstX - textSurface->w / 2;
			dstRect.y = dstY - textSurface->h / 2;
			dstRect.w = textSurface->w;
			dstRect.h = textSurface->h;

			// Render text to screen
			SDL_RenderCopy(renderer, texture, nullptr, &dstRect);
		
			// Get rid of the texture
			SDL_DestroyTexture(texture);
			texture = nullptr;
		}

		// Get rid of the old surface
		SDL_FreeSurface(textSurface);
		textSurface = nullptr;
	}
}

void af::Renderer::Clear()
{
	SDL_RenderClear(renderer);
}

void af::Renderer::Present()
{
	SDL_RenderPresent(renderer);
}