#include "Application.h"

#include <SDL.h>
#include <SDL_image.h>
#include <SDL_mixer.h>
#include <SDL_ttf.h>
#include <iostream>

#include "Timer.h"
#include "ApplicationConfiguration.h"
#include "Game.h"
#include "Renderer.h"
#include "Engine.h"
#include "Window.h"
#include "Content.h"

af::Application::Application(af::ApplicationConfiguration* applicationConfiguration, af::Game* game)
{
	running = true;
	fpsTimer = new af::Timer();
	capTimer = new af::Timer();
	stepTimer = new af::Timer();
	countedFrames = 0;
	fps = applicationConfiguration->fps;
	tickPerFrame = 1000 / fps;
	SDL_Event e;

	af::Engine::application = this;
	if (Initialize(applicationConfiguration))
	{
		game->Initialize();
		fpsTimer->Start();
		while (running)
		{
			capTimer->Start();

			// Handle events on queue
			while (SDL_PollEvent(&e) != 0)
			{
				game->HandleInput(e);
			}

			game->Update();

			// Delta time timer
			stepTimer->Start();

			// Clear screen
			af::Engine::renderer->Clear();

			game->Render(af::Engine::renderer);

			// Update screen
			af::Engine::renderer->Present();

			++countedFrames;

			// Cap framerate with the given FPS cap
			// If frame finished early
			int frameTicks = capTimer->GetTicks();
			if (frameTicks < tickPerFrame)
			{
				// Wait remaining time
				SDL_Delay(tickPerFrame - frameTicks);
			}
		}
		game->Dispose();
	}

	Dispose();
}

af::Application::~Application()
{
}

bool af::Application::IsRunning()
{
	return running;
}

float af::Application::GetDeltaTime()
{
	return stepTimer->GetTicks() / 1000.f;
}

void af::Application::SetFPS(int fps)
{
	this->fps = fps;
	tickPerFrame = 1000 / fps;
}

void af::Application::Quit()
{
	running = false;
}

bool af::Application::InitializeSDL()
{
	// Initialize SDL
	if (SDL_Init(SDL_INIT_EVERYTHING) < 0)
	{
		std::cout << "SDL could not initialize! SDL Error: " << SDL_GetError() << std::endl;
		return false;
	}

	if (!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "0"))
	{
		std::cout << "Warning: Linear texture filtering not enabled!" << std::endl;
		return false;
	}
		
	// Initialize SDL Image
	int imgFlags = IMG_INIT_PNG | IMG_INIT_JPG;
	if (!(IMG_Init(imgFlags) & imgFlags))
	{
		std::cout << "SDL_image could not initialize! SDL_image Error: " << IMG_GetError() << std::endl;
		return false;
	}

	// Initialize SDL Mixer
	if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) < 0)
	{
		std::cout << "SDL_mixer could not initialize! SDL_mixer Error: " << Mix_GetError() << std::endl;
	}

	//Initialize SDL_ttf
	if (TTF_Init() == -1)
	{
		std::cout << "SDL_ttf could not initialize! SDL_ttf Error: " << TTF_GetError() << std::endl;
		return false;
	}

	return true;
}

bool af::Application::InitializeWindow(af::ApplicationConfiguration* applicationConfiguration)
{
	// Create window+
	SDL_Window* sdlWindow = SDL_CreateWindow(applicationConfiguration->title.c_str(), SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, applicationConfiguration->screenWidth, applicationConfiguration->screenHeight, SDL_WINDOW_SHOWN);
	if (sdlWindow == nullptr)
	{
		std::cout << "Window could not be created! SDL Error: " << SDL_GetError() << std::endl;
		return false;
	}

	af::Engine::window = new af::Window(sdlWindow);

	return true;
}

bool af::Application::InitializeRenderer()
{
	// Create renderer for window
	SDL_Renderer* sdlRenderer = SDL_CreateRenderer(af::Engine::window->window, -1, SDL_RENDERER_ACCELERATED);
	if (sdlRenderer == nullptr)
	{
		std::cout << "Renderer could not be created! SDL Error: " << SDL_GetError() << std::endl;
		return false;
	}

	af::Engine::renderer = new af::Renderer(sdlRenderer);
	af::Engine::renderer->SetClearColor(0x00, 0x00, 0x00, 0x00);

	return true;
}

bool af::Application::Initialize(af::ApplicationConfiguration* applicationConfiguration)
{
	if (InitializeSDL())
	{
		if (InitializeWindow(applicationConfiguration))
		{
			if (InitializeRenderer())
			{
				af::Engine::content = new af::Content(af::Engine::renderer->renderer);

				return true;
			}
		}
	}

	return false;
}

void af::Application::Dispose()
{
	// Destroy application systems
	delete af::Engine::content;
	af::Engine::content = nullptr;

	delete af::Engine::renderer;
	af::Engine::renderer = nullptr;

	delete af::Engine::window;
	af::Engine::window = nullptr;
	
	af::Engine::application = nullptr;

	// Quit SDL subsystems
	TTF_Quit();
	Mix_Quit();
	IMG_Quit();
	SDL_Quit();
}