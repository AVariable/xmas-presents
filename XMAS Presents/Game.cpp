#include "Game.h"

#include <ctime>

#include "Renderer.h"
#include "SceneManager.h"
#include "SceneLoading.h"
#include "Engine.h"
#include "Application.h"

af::Game::Game()
{
}

af::Game::~Game()
{
}

void af::Game::Initialize()
{
	std::srand(static_cast<unsigned int>(std::time(nullptr)));
	game::SceneManager::Insert("loading", new game::SceneLoading());
}

void af::Game::Dispose()
{
	game::SceneManager::DeleteAll();
}

void af::Game::HandleInput(SDL_Event& e)
{
	// User clicked window quit button
	if (e.type == SDL_QUIT)
	{
		af::Engine::GetApplication()->Quit();
	}
	else if (e.type == SDL_WINDOWEVENT)
	{
		if (e.window.event == SDL_WINDOWEVENT_FOCUS_GAINED)
		{
			game::SceneManager::SetStateAll(game::Scene::State::ON);
		}
		else if (e.window.event == SDL_WINDOWEVENT_FOCUS_LOST)
		{
			game::SceneManager::SetStateAll(game::Scene::State::OFF);
		}
	}

	game::SceneManager::HandleInput(e);
}

void af::Game::Update()
{
	game::SceneManager::Update();
}

void af::Game::Render(af::Renderer* renderer)
{
	game::SceneManager::Render(renderer);
}