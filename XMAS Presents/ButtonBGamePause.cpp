#include "ButtonBGamePause.h"

#include "Scene.h"
#include "SceneGame.h"
#include "SceneManager.h"
#include "ScenePaused.h"

game::ButtonBGamePause::ButtonBGamePause(SceneGame* sceneGame)
{
	this->sceneGame = sceneGame;
}

game::ButtonBGamePause::~ButtonBGamePause()
{
	sceneGame = nullptr;
}

void game::ButtonBGamePause::OnClick()
{
	sceneGame->SetState(Scene::State::OFF);
	SceneManager::Insert("paused", new ScenePaused(sceneGame));
}