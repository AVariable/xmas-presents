#include "Button.h"

#include "Texture.h"
#include "Text.h"
#include "Rectangle.h"
#include "ButtonBehaviour.h"
#include "Renderer.h"
#include "Sound.h"

game::Button::Button(af::Texture* normalTexture)
{
	enabled = true;
	x = y = xText = yText = 0;
	this->normalTexture = normalTexture;
	pressedTexture = nullptr;
	currentTexture = this->normalTexture;
	text = nullptr;
	rectangle = new af::Rectangle(0, 0, 0, 0);
	pressed = false;
}

game::Button::~Button()
{
	delete normalTexture;
	normalTexture = nullptr;

	if (pressedTexture != nullptr)
	{
		delete pressedTexture;
		pressedTexture = nullptr;
	}

	delete currentTexture;
	currentTexture = nullptr;

	if (text != nullptr)
	{
		delete text;
		text = nullptr;
	}

	delete rectangle;
	rectangle = nullptr;

	delete buttonBehaviour;
	buttonBehaviour = nullptr;

	if (soundClick != nullptr)
	{
		delete soundClick;
		soundClick = nullptr;
	}
}

const float game::Button::GetX()
{
	return x;
}

void game::Button::SetX(float x)
{
	this->x = x;
}

const float game::Button::GetY()
{
	return y;
}

void game::Button::SetY(float y)
{
	this->y = y;
}

const float game::Button::GetXText()
{
	return xText;
}

void game::Button::SetXText(float xText)
{
	this->xText = xText;
}

const float game::Button::GetYText()
{
	return yText;
}

void game::Button::SetYText(float yText)
{
	this->yText = yText;
}

af::Texture* game::Button::GetNormalTexture()
{
	return normalTexture;
}

void game::Button::SetNormalTexture(af::Texture* normalTexture)
{
	this->normalTexture = normalTexture;
}

af::Texture* game::Button::GetPressedTexture()
{
	return pressedTexture;
}

void game::Button::SetPressedTexture(af::Texture* clickedTexture)
{
	this->pressedTexture = clickedTexture;
}

af::Text* game::Button::GetText()
{
	return text;
}

void game::Button::SetText(af::Text* text)
{
	this->text = text;
}

af::Rectangle* game::Button::GetRectangle()
{
	return rectangle;
}

void game::Button::SetRectangle(af::Rectangle* rectangle)
{
	this->rectangle = rectangle;
}

void game::Button::SetButtonBehaviour(ButtonBehaviour* buttonBehaviour)
{
	this->buttonBehaviour = buttonBehaviour;
}

af::Sound* game::Button::GetSoundClick()
{
	return soundClick;
}

void game::Button::SetSoundClick(af::Sound* soundClick)
{
	this->soundClick = soundClick;
}

void game::Button::HandleInput(SDL_Event& e)
{
	if (!enabled)
	{
		return;
	}

	// The a mouse button is pressed
	if (e.type == SDL_MOUSEBUTTONDOWN)
	{
		// Check if the mouse button is the left one
		if (e.button.button == SDL_BUTTON_LEFT)
		{
			af::Rectangle mouseRect(e.button.x, e.button.y, 1, 1);

			// If the mouse pointer intersects the button rectangle
			if (mouseRect.Intersects(rectangle))
			{
				pressed = true;

				// Change the current texture to the pressed texture if it exists
				if (pressedTexture != nullptr)
				{
					currentTexture = pressedTexture;
				}
			}
		}
	}
	// The mouse button is released
	else if (e.type == SDL_MOUSEBUTTONUP)
	{
		// Check if the mouse button is the left one, if it is change the current texture to the normal texture
		if (e.button.button == SDL_BUTTON_LEFT)
		{
			af::Rectangle mouseRect(e.button.x, e.button.y, 1, 1);

			// If the mouse release was inside of the button and a click callback exists
			if (pressed && buttonBehaviour != nullptr && mouseRect.Intersects(rectangle))
			{
				if (soundClick != nullptr)
				{
					soundClick->Play();
				}
				buttonBehaviour->OnClick();
			}

			pressed = false;
			currentTexture = normalTexture;
		}
	}
}

void game::Button::Render(af::Renderer* renderer)
{
	if (!enabled)
	{
		return;
	}

	renderer->Draw(currentTexture, static_cast<int>(x), static_cast<int>(y));

	if (text != nullptr)
	{
		renderer->DrawText(text, static_cast<int>(xText), static_cast<int>(yText));
	}
}