#ifndef PRESENT_H
#define PRESENT_H

#include <SDL.h>
#include <vector>

namespace af
{
	class Rectangle;
	class Texture;
	class Renderer;
}

namespace game
{
	class PresentManager;

	class Present
	{
	public:
		int type;

	protected:
		PresentManager* presentManager;
		bool marked;
		af::Texture* texture;
		af::Texture* textureHover;
		bool hovered;
		float x;
		float y;
		af::Rectangle* rectangle;
		Present* adjacentPresents[4];
		bool transitioning;
		float xTarget;
		float yTarget;
		int score;

	private:
		static const float VELOCITY;

	public:
		Present(PresentManager* presentManager, int width, int height);
		virtual ~Present() = 0;

		bool IsMarked();
		void SetMarked(bool marked);

		float GetX();
		float GetY();
		void SetPosition(float x, float y);

		const af::Rectangle* GetRectangle();

		bool IsTransitioning();

		void SetTargetX(float x);
		void SetTargetY(float y);

		int GetScore();

		// Gets the number of adjacencies of the present
		int GetNAdjacencies();

		// Starts moving the present to the target position
		void MoveToTarget();

		// Inserts an adjacent present if possible
		void InsertAdjacentPresent(Present* present);

		// Clears all the adjacent presents
		void ClearAdjacentPresents();

		// Checks if the present has an adjacent of the same type
		bool HasAdjacentOfSameType();

		// Gets all the presents of the same type depending on the type of present
		virtual void GetPresentsOfType(int type, std::vector<Present*>& presents);

		// Marks this present for deletion and any adjacent presents of the same type
		void MarkForDeletion();

		void HandleInput(SDL_Event& e);
		void Update();
		void Render(af::Renderer* renderer);

	protected:
		// Deletes the given present from the adjacent ones
		void DeleteAdjacentPresent(Present* present);
	};
}

#endif