#include "ButtonBGameAddColumn.h"

#include "SceneGame.h"

game::ButtonBGameAddColumn::ButtonBGameAddColumn(SceneGame* sceneGame)
{
	this->sceneGame = sceneGame;
}

game::ButtonBGameAddColumn::~ButtonBGameAddColumn()
{
	sceneGame = nullptr;
}

void game::ButtonBGameAddColumn::OnClick()
{
	sceneGame->InsertPresentsFirstColumn();
}