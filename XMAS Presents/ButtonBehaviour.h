#ifndef BUTTON_BEHAVIOUR_H
#define BUTTON_BEHAVIOUR_H

namespace game
{
	class ButtonBehaviour
	{
	public:
		virtual void OnClick() = 0;
	};
}

#endif