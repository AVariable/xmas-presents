#include "ButtonBScoreQuit.h"

#include "SceneManager.h"
#include "SceneMainMenu.h"
#include "Music.h"

game::ButtonBScoreQuit::ButtonBScoreQuit()
{
}

game::ButtonBScoreQuit::~ButtonBScoreQuit()
{
}

void game::ButtonBScoreQuit::OnClick()
{
	af::Music::Stop();
	SceneManager::DeleteAll();
	SceneManager::Insert("mainmenu", new SceneMainMenu());
}