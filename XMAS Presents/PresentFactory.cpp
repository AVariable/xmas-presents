#include "PresentFactory.h"

#include <ctime>

#include "Present.h"
#include "PresentManager.h"
#include "SceneGame.h"
#include "PresentDefault.h"
#include "PresentBag.h"

game::Present* game::PresentFactory::CreatePresent(PresentManager* presentManager, int width, int height)
{
	int level = presentManager->GetSceneGame()->GetLevel();

	while (true)
	{
		// Calculate probability to spawn certain types of presents
		int probability = std::rand() % 100 + 1;

		// Spawn default present
		if (probability <= 95)
		{
			return new PresentDefault(presentManager, width, height);
		}
		// Spawn bag present
		else if (probability <= 100 && level >= 4)
		{
			return new PresentBag(presentManager, width, height);
		}
	}
}