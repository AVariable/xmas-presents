#include "SceneTutorialElf.h"

#include "Renderer.h"
#include "Texture.h"
#include "Text.h"
#include "Content.h"
#include "Engine.h"
#include "Timer.h"
#include "SceneManager.h"

const int game::SceneTutorialElf::MAX_TIME = 3000;

game::SceneTutorialElf::SceneTutorialElf()
{
}

game::SceneTutorialElf::~SceneTutorialElf()
{
	delete textureBalloon;
	textureBalloon = nullptr;
	delete textBalloon;
	textBalloon = nullptr;
}

void game::SceneTutorialElf::Initialize()
{
	af::Content* content = af::Engine::GetContent();

	textureBalloon = new af::Texture(content->GetTexture("spriteSheet"), 150, 75, 138, 105, 50, 50);
	textureBalloon->SetBlendMode(SDL_BLENDMODE_BLEND);
	textureBalloon->SetColor(255, 255, 255, 255);

	textBalloon = new af::Text(content->GetFont("8bit14"));
	textBalloon->SetColor(0, 0, 0, 0);
	textBalloon->SetText("Click Me");

	content = nullptr;

	timer = new af::Timer();
	timer->Start();
}

void game::SceneTutorialElf::HandleInput(SDL_Event& e)
{
}

void game::SceneTutorialElf::Update()
{
	if (timer->GetTicks() > MAX_TIME)
	{
		SceneManager::Delete(this);
	}
}

void game::SceneTutorialElf::Render(af::Renderer* renderer)
{
	renderer->Draw(textureBalloon, 170, 445);
	renderer->DrawText(textBalloon, 175, 440);
}