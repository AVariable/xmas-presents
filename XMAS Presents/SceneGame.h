#ifndef SCENE_GAME_H
#define SCENE_GAME_H

#include "Scene.h"

namespace af
{
	class Texture;
	class Timer;
	class Renderer;
	class Text;
	class Sound;
}

namespace game
{
	class PresentManager;
	class Button;

	class SceneGame : public Scene
	{
	private:
		static const int TIME_TO_SPAWN;
		static const float Y_MIN_CANDY_CANE;
		static const float Y_MAX_CANDY_CANE;
		static const float VELOCITY_CANDY_CANE;
		static const int INITIAL_SCORE_TO_NEXT_LEVEL;
		static const float Y_START_TREE;

		af::Texture* textureBackground;
		af::Texture* textureWall;
		PresentManager* presentManager;
		af::Timer* timer;
		Button* buttonAddColumn;
		af::Text* textScore;
		af::Text* textTotalScore;
		int totalScore;
		af::Texture* textureTree;
		bool showAddColumn;
		af::Texture* textureCandyCane;
		bool candyCaneUp;
		float yCandyCane;
		int scoreToNextLevel;
		bool presentsRespawning;
		af::Text* textTitleScoreToNextLevel;
		af::Text* textScoreToNextLevel;
		Button* buttonQuit;
		int level;
		Button* buttonPause;
		af::Sound* soundAddColumnWarning;
		af::Sound* soundAddColumn;
		af::Text* textLevel;
		af::Text* textLevelNumber;
		bool firstElf;
		bool firstPush;

	public:
		SceneGame();
		~SceneGame();

		void SetState(State state);

		int GetTotalScore();

		void SetShowAddColumn(bool showAddColumn);

		int GetLevel();

		// Resets the timer and inserts new presents in the first column if possible
		void InsertPresentsFirstColumn();

		// Adds score to the total score
		void AddToScore(int score);

		void PauseTimer();

		void UnpauseTimer();

	protected:
		void Initialize();
		void HandleInput(SDL_Event& e);
		void Update();
		void Render(af::Renderer* renderer);
	};
}

#endif