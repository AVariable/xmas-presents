#include "ButtonBMainMenuPlay.h"

#include "SceneManager.h"
#include "Scene.h"
#include "SceneGame.h"
#include "SceneTutorialStart.h"
#include "Music.h"

game::ButtonBMainMenuPlay::ButtonBMainMenuPlay()
{
}

game::ButtonBMainMenuPlay::~ButtonBMainMenuPlay()
{
}

void game::ButtonBMainMenuPlay::OnClick()
{
	af::Music::Stop();
	SceneManager::Delete("mainmenu");

	SceneGame* sceneGame = new SceneGame();
	sceneGame->SetState(Scene::State::FROZEN);

	SceneManager::Insert("game", sceneGame);
	SceneManager::Insert("tutorialstart", new SceneTutorialStart(sceneGame));
}