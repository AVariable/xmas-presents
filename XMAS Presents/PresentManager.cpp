#include "PresentManager.h"

#include "Renderer.h"
#include "SceneGame.h"
#include "Present.h"
#include "PresentFactory.h"
#include "Rectangle.h"
#include "Engine.h"
#include "Content.h"
#include "Sound.h"

const unsigned int game::PresentManager::X_PRESENTS = 10;
const unsigned int game::PresentManager::PRESENT_WIDTH = 51;
const unsigned int game::PresentManager::PRESENT_HEIGHT = 53;

game::PresentManager::PresentManager(SceneGame* sceneGame, float xStart, float yStart)
	:xStart(xStart), yStart(yStart)
{
	clickPresentSound = new af::Sound(af::Engine::GetContent()->GetSound("clickPresent"));
	std::fill_n(presents, SIZE_PRESENTS, nullptr);
	this->sceneGame = sceneGame;
	ClearAndRespawn();
}

game::PresentManager::~PresentManager()
{
	// Delete all presents
	for (int i = 0; i < SIZE_PRESENTS; i++)
	{
		if (presents[i] != nullptr)
		{
			delete presents[i];
			presents[i] = nullptr;
		}
	}
}

game::SceneGame* game::PresentManager::GetSceneGame()
{
	return sceneGame;
}

game::Present** game::PresentManager::GetPresents()
{
	return presents;
}

void game::PresentManager::ClearAndRespawn()
{
	// Loop through the presents list and delete any remaining present
	for (unsigned int i = 0; i < SIZE_PRESENTS; i++)
	{
		if (presents[i] != nullptr)
		{
			delete presents[i];
			presents[i] = nullptr;
		}
	}
	
	float yCurrent = yStart;

	// Creates all the initial presents (half of the grid)
	for (unsigned int i = 0; i < SIZE_PRESENTS / X_PRESENTS; i++)
	{
		float xCurrent = xStart - SIZE_PRESENTS / X_PRESENTS * PRESENT_WIDTH;

		for (unsigned int j = i * X_PRESENTS; j < i * X_PRESENTS + X_PRESENTS / 2; j++)
		{
			presents[j] = PresentFactory::CreatePresent(this, PRESENT_WIDTH, PRESENT_HEIGHT);
			presents[j]->SetPosition(xCurrent, yCurrent);

			xCurrent += PRESENT_WIDTH;
		}

		yCurrent += PRESENT_HEIGHT;
	}

	// Calculate adjacencies for all the presents
	RecalculateAdjacencies();

	// Repositions all presents
	RepositionPresents();
}

void game::PresentManager::HandleInput(SDL_Event& e)
{
	// Loop through all the presents and handle their input
	for (unsigned int i = 0; i < SIZE_PRESENTS; i++)
	{
		if (presents[i] != nullptr)
		{
			presents[i]->HandleInput(e);
		}
	}

	if (e.type == SDL_MOUSEBUTTONUP)
	{
		if (e.button.button == SDL_BUTTON_LEFT)
		{
			af::Rectangle mouseRect(e.button.x, e.button.y, 1, 1);

			for (unsigned int i = 0; i < SIZE_PRESENTS; i++)
			{
				if (presents[i] != nullptr)
				{
					// Check if the mouse rectangle intersected with a present
					if (mouseRect.Intersects(presents[i]->GetRectangle()))
					{
						// Check if the intersected present has adjacent presents of the same type
						if (presents[i]->HasAdjacentOfSameType())
						{
							clickPresentSound->Play();

							// Mark the presents for deletion and add score
							presents[i]->MarkForDeletion();

							// Deletes all marked for deletion presents
							DeleteAllMarked();

							// Reposition the presents on screen and in the array on the y axis
							RepositionYAxis();

							// Reposition the presents on screen and in the array on the x axis
							RepositionXAxis();
							
							// Clear adjacent presents of all the presents
							ClearAllAdjacencies();

							// Recalculate adjacent presents
							RecalculateAdjacencies();
							
							// Moves the presents to their positions on the grid
							RepositionPresents();

							// Checks if exists adjacencies of the same the type
							sceneGame->SetShowAddColumn(!ExistsAdjacenciesOfSameType());

							break;
						}
					}
				}
			}
		}
	}
}

void game::PresentManager::Update()
{
	// Updates presents that exist
	for (unsigned int i = 0; i < SIZE_PRESENTS; i++)
	{
		if (presents[i] != nullptr)
		{
			presents[i]->Update();
		}
	}
}

void game::PresentManager::Render(af::Renderer* renderer)
{
	// Renders presents that exist
	for (unsigned int i = 0; i < SIZE_PRESENTS; i++)
	{
		if (presents[i] != nullptr)
		{
			presents[i]->Render(renderer);
		}
	}
}

void game::PresentManager::DeleteAllMarked()
{
	int totalScore = 0;
	int totalPresents = 0;

	// Loop through all the presents
	for (unsigned int i = 0; i < SIZE_PRESENTS; i++)
	{
		// If the present is not a null pointer
		if (presents[i] != nullptr)
		{
			// If the present is marked for deletion
			if (presents[i]->IsMarked())
			{
				// Add to the total score and add a present
				totalScore += presents[i]->GetScore();
				totalPresents++;

				// Delete the present
				delete presents[i];
				presents[i] = nullptr;
			}
		}
	}

	// If the total number of presents is greater than 2, add a bonus to the total score
	if (totalPresents > 2)
	{
		totalScore += totalScore;
	}

	sceneGame->AddToScore(totalScore * sceneGame->GetLevel());
}

void game::PresentManager::RepositionYAxis()
{
	for (int i = SIZE_PRESENTS - X_PRESENTS - 1; i >= 0; i--)
	{
		if (presents[i] != nullptr)
		{
			int iCurrent = i;
			int positionsToMove = 0;
			bool lastPosition = false;

			// Loop through all the available slots below the current present
			do
			{
				iCurrent += X_PRESENTS;

				// If the slot to access is bigger than the size of the array exit the loop
				if (iCurrent > SIZE_PRESENTS - 1)
				{
					lastPosition = true;
				}
				else
				{
					// If the slot is available, add a position to move
					if (presents[iCurrent] == nullptr)
					{
						positionsToMove++;
					}
				}
			} while (!lastPosition);

			// If the present needs to move
			if (positionsToMove > 0)
			{
				// Save the present in a temporary pointer
				Present* present = presents[i];

				// Assign the new position of the present
				presents[i + positionsToMove * X_PRESENTS] = present;

				// Assign null pointer to the old position
				presents[i] = nullptr;

				present = nullptr;
			}
		}
	}
}

void game::PresentManager::RepositionXAxis()
{
	for (int i = SIZE_PRESENTS - X_PRESENTS + 1; i < SIZE_PRESENTS; i++)
	{
		if (presents[i] != nullptr)
		{
			int iCurrent = i;
			int columnsToMove = 0;
			bool lastPosition = false;

			// Loop through all the available slots before the current column
			do
			{
				iCurrent -= 1;

				// If the slot to access is lesser than the last number of rows
				if (iCurrent < SIZE_PRESENTS - X_PRESENTS)
				{
					lastPosition = true;
				}
				else
				{
					// If the slot is available, add a position to move
					if (presents[iCurrent] == nullptr)
					{
						columnsToMove++;
					}
				}
			} while (!lastPosition);

			// If the present needs to move
			if (columnsToMove > 0)
			{
				int iPresent = i;

				do
				{
					// Save the present in a temporary pointer
					Present* present = presents[iPresent];

					if (present != nullptr)
					{
						// Assign the new position of the present
						presents[iPresent - columnsToMove] = present;

						// Assign null pointer to the old position
						presents[iPresent] = nullptr;
					}

					present = nullptr;
					iPresent -= X_PRESENTS;
				} while (iPresent > 0);
			}
		}
	}
}

void game::PresentManager::ClearAllAdjacencies()
{
	// Clears adjacencies of all presents
	for (unsigned int i = 0; i < SIZE_PRESENTS; i++)
	{
		if (presents[i] != nullptr)
		{
			presents[i]->ClearAdjacentPresents();
		}
	}
}

void game::PresentManager::RecalculateAdjacencies()
{
	for (unsigned int i = 0; i < SIZE_PRESENTS - 1; i++)
	{
		// Check if the current position is a null pointer
		if (presents[i] != nullptr)
		{
			// If the present is in the bottom row
			if (i >= SIZE_PRESENTS - X_PRESENTS)
			{
				// Check if the next present is not a null pointer
				if (presents[i + 1] != nullptr)
				{
					// Insert the next present as an adjacent of the current one
					presents[i]->InsertAdjacentPresent(presents[i + 1]);
				}
			}
			else
			{
				// If the present is not the last of the row
				if ((i + 1) % X_PRESENTS != 0)
				{
					// Check if the next present is not a null pointer
					if (presents[i + 1] != nullptr)
					{
						// Insert the next present as an adjacent of the current one
						presents[i]->InsertAdjacentPresent(presents[i + 1]);
					}

					// Check if the present below the current one is not a null pointer
					if (presents[i + X_PRESENTS] != nullptr)
					{
						// Insert the present that is below as an adjacent of the current one
						presents[i]->InsertAdjacentPresent(presents[i + X_PRESENTS]);
					}
				}
				else
				{
					// Check if the present below the current one is not a null pointer
					if (presents[i + X_PRESENTS] != nullptr)
					{
						// Inser the present that is below as an adjacent of the current one
						presents[i]->InsertAdjacentPresent(presents[i + X_PRESENTS]);
					}
				}
			}
		}
	}
}

bool game::PresentManager::IsLastColumnOccupied()
{
	return presents[SIZE_PRESENTS - 1] != nullptr;
}

void game::PresentManager::SpawnNewPresents()
{
	PushColumnsForward();

	float yCurrent = yStart;
	for (unsigned int i = 0; i < SIZE_PRESENTS / X_PRESENTS; i++)
	{
		int iCurrentPresent = i * X_PRESENTS;
		int iNextPresent = iCurrentPresent + 1;

		presents[iCurrentPresent] = PresentFactory::CreatePresent(this, PRESENT_WIDTH, PRESENT_HEIGHT);

		// If the next present exists
		if (presents[iNextPresent] != nullptr)
		{
			presents[iCurrentPresent]->SetPosition(presents[iNextPresent]->GetX() - PRESENT_WIDTH, yCurrent);
		}
		else
		{
			presents[iCurrentPresent]->SetPosition(xStart - PRESENT_WIDTH, yCurrent);
		}

		yCurrent += PRESENT_HEIGHT;
	}

	// Create adjacency between the new presents and the old ones
	for (unsigned int i = 0; i < SIZE_PRESENTS / X_PRESENTS; i++)
	{
		int iCurrentPresent = i * X_PRESENTS;
		int iNextPresent = i * X_PRESENTS + 1;
		int iBelowPresent = (i + 1) * X_PRESENTS;

		// If the present right next to the current present is not a null pointer, insert it into the adjacent list of the current present
		if (presents[iNextPresent] != nullptr)
		{
			presents[iCurrentPresent]->InsertAdjacentPresent(presents[iNextPresent]);
		}

		// If it's not the last row, insert the present below the current one to the adjacent list of the current present
		if (i < SIZE_PRESENTS / X_PRESENTS - 1)
		{
			presents[iCurrentPresent]->InsertAdjacentPresent(presents[iBelowPresent]);
		}
	}

	RepositionPresents();
}

void game::PresentManager::RepositionPresents()
{
	for (unsigned int i = 0; i < SIZE_PRESENTS; i++)
	{
		// If the present exists
		if (presents[i] != nullptr)
		{
			// Reposition the presents based on it's position on the array
			float xTarget = xStart + (i % X_PRESENTS * PRESENT_WIDTH);
			float yTarget = yStart + (i / X_PRESENTS * PRESENT_HEIGHT);

			presents[i]->SetTargetX(xTarget);
			presents[i]->SetTargetY(yTarget);
			presents[i]->MoveToTarget();
		}
	}
}

bool game::PresentManager::ExistsAdjacenciesOfSameType()
{
	for (unsigned int i = 0; i < SIZE_PRESENTS; i++)
	{
		if (presents[i] != nullptr)
		{
			if (presents[i]->HasAdjacentOfSameType())
			{
				return true;
			}
		}
	}

	return false;
}

bool game::PresentManager::CheckIfTransitioning()
{
	for (unsigned int i = 0; i < SIZE_PRESENTS; i++)
	{
		if (presents[i] != nullptr)
		{
			if (presents[i]->IsTransitioning())
			{
				return true;
			}
		}
	}

	return false;
}

void game::PresentManager::PushColumnsForward()
{
	// Loop through the bottom presents from last to first
	for (unsigned int i = SIZE_PRESENTS - 2; i >= SIZE_PRESENTS - X_PRESENTS; i--)
	{
		// If the present exists, move the the column forward
		if (presents[i] != nullptr)
		{
			int iPresent = i;

			do
			{
				// Save the present in a temporary pointer
				Present* present = presents[iPresent];

				if (present != nullptr)
				{
					// Assign the new position of the present
					presents[iPresent + 1] = present;

					// Assign null pointer to the old position
					presents[iPresent] = nullptr;
				}

				present = nullptr;
				iPresent -= X_PRESENTS;
			} while (iPresent >= 0);
		}
	}
}