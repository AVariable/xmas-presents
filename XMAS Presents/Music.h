#ifndef MUSIC_H
#define MUSIC_H

#include <SDL_mixer.h>

namespace af
{
	class Music
	{
	public:
		// Checks if a music is playing
		static bool IsPlaying();
		
		// Sets the volume of the music player
		static void SetVolume(int volume);

		// Overwrites and plays this music
		static void Play(Mix_Music* music);

		// Stops the current playing music
		static void Stop();
	};
}

#endif