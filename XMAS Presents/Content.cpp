#include "Content.h"

#include <iostream>

af::Content::Content(SDL_Renderer* renderer)
{
	this->renderer = renderer;
}

af::Content::~Content()
{
	// Destroy all textures
	for (std::pair<std::string, SDL_Texture*> pair : textures)
	{
		SDL_DestroyTexture(pair.second);
		pair.second = nullptr;
	}
	textures.clear();

	// Destroy all fonts
	for (std::pair<std::string, TTF_Font*> pair : fonts)
	{
		TTF_CloseFont(pair.second);
		pair.second = nullptr;
	}
	fonts.clear();

	// Destroy all sounds
	for (std::pair<std::string, Mix_Chunk*> pair : sounds)
	{
		Mix_FreeChunk(pair.second);
		pair.second = nullptr;
	}

	renderer = nullptr;
}

SDL_Texture* af::Content::GetTexture(std::string name)
{
	// Search for a texture with the same name
	auto iterator = textures.find(name);

	// Check if the texture exists and return it
	if (iterator != textures.end())
	{
		return iterator->second;
	}

	return nullptr;
}

Mix_Chunk* af::Content::GetSound(std::string name)
{
	// Search for a sound with the same name
	auto iterator = sounds.find(name);

	// Check if the sound exists and return it
	if (iterator != sounds.end())
	{
		return iterator->second;
	}

	return nullptr;
}

TTF_Font* af::Content::GetFont(std::string name)
{
	// Search for a font with the same name
	auto iterator = fonts.find(name);

	// Check if the font exists and return it
	if (iterator != fonts.end())
	{
		return iterator->second;
	}

	return nullptr;
}

Mix_Music* af::Content::GetMusic(std::string name)
{
	// Search for a music with the same name
	auto iterator = music.find(name);

	// Check if the music exists and return it
	if (iterator != music.end())
	{
		return iterator->second;
	}

	return nullptr;
}

void af::Content::LoadTexture(std::string path, std::string name)
{
	// Search for a texture with the same name
	auto pos = textures.find(name);
	
	// Check if the texture doesn't exist
	if (pos == textures.end())
	{
		SDL_Texture* sdlTexture = nullptr;

		// Load the image from the specified path
		SDL_Surface* loadedSurface = IMG_Load(path.c_str());

		// Check if the image was loaded correctly
		if (loadedSurface == nullptr)
		{
			std::cout << "Unable to load image " << path.c_str() << "! SDL_image Error : " << IMG_GetError() << std::endl;
		}
		else
		{
			// Create a texture from the surface pixels of the loaded image
			sdlTexture = SDL_CreateTextureFromSurface(renderer, loadedSurface);
			
			// Get rid of the loaded surface
			SDL_FreeSurface(loadedSurface);
			loadedSurface = nullptr;

			// If the creation of the texture was unsuccessful
			if (sdlTexture == nullptr)
			{
				std::cout << "Unable to create texture from " << path << "! SDL Error: " << SDL_GetError() << std::endl;
			}
			else
			{
				textures.insert(std::make_pair(name, sdlTexture));
			}
		}
	}
}

void af::Content::LoadFont(std::string path, std::string name, int size)
{
	// Search for a font with the same name
	auto pos = fonts.find(name);

	// Check if the font doesn't exist
	if (pos == fonts.end())
	{
		TTF_Font* font = TTF_OpenFont(path.c_str(), size);

		// Check if the font was not loaded
		if (font == nullptr)
		{
			std::cout << "Unable to load font from " << path << "! SDL_ttf Error " << TTF_GetError() << std::endl;
		}
		else
		{
			fonts.insert(std::make_pair(name, font));
		}
	}
}

void af::Content::LoadSound(std::string path, std::string name)
{
	// Search for a sound with the same name
	auto pos = sounds.find(name);

	// Check if the sound doesn't exist
	if (pos == sounds.end())
	{
		Mix_Chunk* sound = Mix_LoadWAV(path.c_str());

		// Check if the sound was not loaded
		if (sound == nullptr)
		{
			std::cout << "Unable to load sound from " << path << "! SDL_Mixer Error " << Mix_GetError() << std::endl;
		}
		else
		{
			sounds.insert(std::make_pair(name, sound));
		}
	}
}

void af::Content::LoadMusic(std::string path, std::string name)
{
	// Search for a music with the same name
	auto pos = this->music.find(name);

	// Check if the music doesn't exist
	if (pos == this->music.end())
	{
		Mix_Music* music = Mix_LoadMUS(path.c_str());

		// Check if the music was not loaded
		if (music == nullptr)
		{
			std::cout << "Unable to load music from " << path << "! SDL_Mixer Error " << Mix_GetError() << std::endl;
		}
		else
		{
			this->music.insert(std::make_pair(name, music));
		}
	}
}