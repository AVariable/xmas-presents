#include "Music.h"

bool af::Music::IsPlaying()
{
	return Mix_PlayingMusic() != 0;
}

void af::Music::SetVolume(int volume)
{
	// Sets the volume to a value between 0 and 128
	Mix_VolumeMusic(volume < 0 ? 0 : volume > MIX_MAX_VOLUME ? MIX_MAX_VOLUME : volume);
}

void af::Music::Play(Mix_Music* music)
{
	Mix_PlayMusic(music, -1);
}

void af::Music::Stop()
{
	Mix_HaltMusic();
}