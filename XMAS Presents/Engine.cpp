#include "Engine.h"

#include "Application.h"
#include "Window.h"
#include "Renderer.h"
#include "Content.h"

af::Application* af::Engine::application = nullptr;
af::Window* af::Engine::window = nullptr;
af::Renderer* af::Engine::renderer = nullptr;
af::Content* af::Engine::content = nullptr;

af::Application* af::Engine::GetApplication()
{
	return application;
}

af::Window* af::Engine::GetWindow()
{
	return window;
}

af::Content* af::Engine::GetContent()
{
	return content;
}