#include "Text.h"

#include "Renderer.h"

af::Text::Text(TTF_Font* font)
{
	this->font = font;
	text = "Default";
	red = green = blue = 255;
	alpha = 0;
}

af::Text::~Text()
{
}

std::string af::Text::GetText()
{
	return text;
}

void af::Text::SetText(std::string text)
{
	this->text = text;
}

void af::Text::SetColor(Uint8 red, Uint8 green, Uint8 blue, Uint8 alpha)
{
	this->red = red;
	this->green = green;
	this->blue = blue;
	this->alpha = alpha;
}